// https://adventofcode.com/2021/day/8

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "../common.c"

#if 0
#define INPUT_FILE "sample.txt"
#else
#define INPUT_FILE "input.txt"
#endif

#define DEBUG 0


uint8_t nums_part1[4] = {2, 3, 4, 7};
uint8_t solved[10];
int8_t map[128];

char *chop(char *input, char delim, size_t *len)
{
        while (*len) {
                (*len)--;
                if (*input == delim) {
                        *input = '\0';
                        break;
                }
                input++;
        }
        return input + 1;
}


void part1(void)
{
        char *input, *begin, *next_line, *next_seg;
        size_t answer, input_len, line_len, seg_len;
        uint8_t i;

        input = read_input(INPUT_FILE);
        begin = input;
        input_len = strlen(input);
        answer = 0;

        while (input_len > 0) {
                next_line = chop(input, '\n', &input_len);
                input = strchr(input, '|') + 2;
                line_len = strlen(input);
                while (line_len > 0) {
                        next_seg = chop(input, ' ', &line_len);
                        seg_len = strlen(input);
                        for (i = 0; i < 4; ++i) {
                                answer += seg_len == nums_part1[i];
                        }
                        input = next_seg;
                }
                input = next_line;
        }

        printf("Part 1: %zu\n", answer);
        free(begin);
}

void print_byte(uint8_t byte)
{
        for (uint8_t mask = 0x80; mask > 0; mask >>= 1) {
                printf("%u", (byte & mask) && mask);
        }
        printf("\n");
}

uint8_t count_ones(uint8_t byte)
{
        uint8_t ones = 0;
        for (uint8_t mask = 0x80; mask > 0; mask >>= 1) {
                ones += (byte & mask) && mask;
        }
        return ones;
}

void solve_signals(uint8_t signals[10])
{
        uint8_t i, ones, fives[3], sixes[3], fives_i, sixes_i;
        fives_i = sixes_i = 0;

        // Solve 1, 4, 7, 8 and sort the rest into two piles of five and six segments
        for (i = 0; i < 10; ++i) {
                ones = count_ones(signals[i]);
                if (ones == 2) {
                        solved[1] = signals[i];
                } else if (ones == 3) {
                        solved[7] = signals[i];
                } else if (ones == 4) {
                        solved[4] = signals[i];
                } else if (ones == 5) {
                        fives[fives_i++] = signals[i];
                } else if (ones == 6) {
                        sixes[sixes_i++] = signals[i];
                } else if (ones == 7) {
                        solved[8] = signals[i];
                } else {
                        printf("Unreachable\n");
                }
        }

        // Solve for 9
        for (i = 0; i < 3; ++i) {
                if ((sixes[i] & solved[4]) == solved[4]) {
                        solved[9] = sixes[i];
                        if (i < 2) {    // tidy after yourself
                                sixes[i] = sixes[2];
                        }
                        break;
                }
        }

        // Solve for 0, and inevitably 6
        solved[0] = (sixes[0] & solved[7]) == solved[7] ? sixes[0]: sixes[1];
        solved[6] = solved[0] == sixes[0] ? sixes[1]: sixes[0];

        // Solve for 3
        for (i = 0; i < 3; ++i) {
                if ((fives[i] & solved[7]) == solved[7]) {
                        solved[3] = fives[i];
                        if (i < 2) {    // tidy after yourself
                                fives[i] = fives[2];
                        }
                        break;
                }
        }

        // Solve for rest
        solved[2] = (solved[4] | fives[0]) == solved[8] ? fives[0] : fives[1];
        solved[5] = solved[2] == fives[0] ? fives[1] : fives[0];

        if (DEBUG) {
                for (i = 0; i < 10; ++i) {
                        printf("%3u: ", i);
                        print_byte(solved[i]);
                }
        }

}

// Compress one string of 2 - 7 segments into bits of single byte
uint8_t compress_signal(char *str)
{
        uint8_t byte = 0;
        size_t len = strlen(str);
        for (uint8_t i = 0; i < len; ++i) {
                byte += 1 << (str[i] - 'a');
        }
        return byte;
}


void part2(void)
{
        char *input, *output, *begin, *next_line, *next_num;
        size_t total_len, sub_len, answer;
        uint16_t multiplier, line_cnt;
        uint8_t signals[10], i;

        input = read_input(INPUT_FILE);
        begin = input;
        total_len = strlen(input);
        line_cnt = answer = 0;

        while (total_len > 0) {
                // Parse input
                memset(map, -1, 128);
                i = 0;
                multiplier = 1000;
                next_line = chop(input, '\n', &total_len);
                output = strchr(input, '|') + 2;
                output[-3] = '\0';
                sub_len = strlen(input);

                while (sub_len > 0) {
                        next_num = chop(input, ' ', &sub_len);
                        signals[i++] = compress_signal(input);
                        input = next_num;
                }

                solve_signals(signals);
                for (i = 0; i < 10; ++i) {
                        map[solved[i]] = i;
                }

                if (DEBUG) {
                        printf("Line %u:\n", ++line_cnt);
                        for (i = 0; i < 128; ++i) {
                                if (i % 10 == 0) {
                                        printf("%3u | ", i);
                                }
                                if (map[i] >= 0) {
                                        printf("(%d)", map[i]);
                                } else {
                                        printf("%3d", map[i]);
                                }
                                if (i % 10 == 9) {
                                        printf("\n");
                                }
                        }
                        printf("\n");
                }

                // Parse output
                sub_len = strlen(output);
                size_t line_sum = 0;
                size_t num;

                while (sub_len > 0) {
                        num = 0;
                        next_num = chop(output, ' ', &sub_len);
                        if (DEBUG) { printf("%s ", output); }
                        num = compress_signal(output);
                        num = map[num];
                        line_sum += num * multiplier;
                        multiplier /= 10;
                        output = next_num;
                }

                answer += line_sum;
                input = next_line;

                if (DEBUG) {
                        printf("\nLine sum: %zu\n", line_sum);
                        printf("Cumulative answer: %zu\n", answer);
                }
        }

        printf("Part 2: %zu\n", answer);

        free(begin);
}

int main(void)
{
        part1();
        part2();
        return 0;
}
