// https://adventofcode.com/2021/day/5

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <stdbool.h>
#include "../common.c"

#if 0
#define INPUT_FILE "sample.txt"
#define LEN 10
#define CAP 10
#else
#define INPUT_FILE "input.txt"
#define LEN 500
#define CAP 1000
#endif

typedef struct line {
        uint16_t pos[2][2];
} Line;

uint16_t arr[CAP][CAP];
Line lines[LEN];

void parse_input(char *input, Line lines[LEN])
{
        for (uint16_t line = 0; line < LEN; ++line) {
                for (uint16_t co = 0; co < 2; ++co) {
                        for (uint16_t value = 0; value < 2; ++value) {
                                lines[line].pos[co][value] = atoi(input);
                                while(isdigit(*input++));
                        }
                        while(!(isdigit(*input)) && !(*input == '\0')) { input++; }
                }
        }
}


void plug_lines(Line lines[LEN], uint16_t arr[CAP][CAP], bool diag)
{
        uint16_t x1, x2, y1, y2, x_sign, y_sign;
        for (uint16_t line = 0; line < LEN; ++line) {
                x1 = lines[line].pos[0][0];
                x2 = lines[line].pos[1][0];
                y1 = lines[line].pos[0][1];
                y2 = lines[line].pos[1][1];
                if (!diag) {
                        if (!((x1 == x2) || (y1 == y2)))
                                continue;
                }
                x_sign = x1 - x2 < 0 ? 1 : -1;
                y_sign = y1 - y2 < 0 ? 1 : -1;
                while ((x1 != x2) || (y1 != y2)) {
                        arr[y1][x1]++;
                        x1 += x1 == x2 ? 0 : x_sign;
                        y1 += y1 == y2 ? 0 : y_sign;
                }
                arr[y1][x1]++;
        }
}

void print_map(uint16_t arr[CAP][CAP])
{
        for (uint16_t y = 0; y < CAP; ++y) {
                for (uint16_t x = 0; x < CAP; ++x) {
                        if (arr[y][x] == 0) {
                                printf(".");
                        } else {
                                printf("%u", arr[y][x]);
                        }
                }
                printf("\n");
        }
        printf("\n");
}

uint16_t count_overlaps(uint16_t arr[CAP][CAP])
{
        uint16_t count = 0;
        for (uint16_t y = 0; y < CAP; ++y) {
                for (uint16_t x = 0; x < CAP; ++x) {
                        if (arr[y][x] > 1) {
                                count++;
                        }
                }
        }
        return count;
}

void part1(void)
{
        char *input = read_input(INPUT_FILE);
        memset(arr, 0, sizeof(arr));
        memset(lines, 0, sizeof(lines));
        parse_input(input, lines);
        plug_lines(lines, arr, false);
        /* print_map(arr); */
        free(input);
        printf("Part 1: %u\n", count_overlaps(arr));
}

void part2(void)
{
        char *input = read_input(INPUT_FILE);
        memset(arr, 0, sizeof(arr));
        memset(lines, 0, sizeof(lines));
        parse_input(input, lines);
        plug_lines(lines, arr, true);
        /* print_map(arr); */
        free(input);
        printf("Part 2: %u\n", count_overlaps(arr));
}

int main(void)
{
        part1();
        part2();
        return 0;
}
