// https://adventofcode.com/2021/day/9

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "../common.c"

#if 0
#define INPUT_FILE "sample.txt"
#define X 10
#define Y 5
#else
#define INPUT_FILE "input.txt"
#define X 100
#define Y 100
#endif

#define BASINS_N 256

void print_lowest(uint8_t table[Y][X], uint8_t lowest[Y][X])
{
        uint8_t x, y;
        for (y = 0; y < Y; ++y, putchar('\n'))
                for (x = 0; x < X; ++x)
                        lowest[y][x] ?
                                printf("(%u)", table[y][x]) :
                                printf(" %u ", table[y][x]);
}

void init_table(char *input, uint8_t table[Y][X])
{
        uint8_t x, y;
        for (y = 0; y < Y; ++y, ++input)
                for (x = 0; x < X; ++x, ++input)
                        table[y][x] = *input - '0';
}

void mark_lowest(uint8_t table[Y][X], uint8_t lowest[Y][X])
{
        uint8_t x, y, cur, n, e, s, w;
        for (y = 0; y < Y; ++y)
                for (x = 0; x < X; ++x) {
                        cur = table[y][x];
                        n   = y > 0     ? table[y-1][x] : ~0;
                        e   = x < X - 1 ? table[y][x+1] : ~0;
                        s   = y < Y - 1 ? table[y+1][x] : ~0;
                        w   = x > 0     ? table[y][x-1] : ~0;
                        if (cur < n && cur < e && cur < s && cur < w)
                                lowest[y][x] = 1;
                }
}

uint64_t count_lowest(uint8_t table[Y][X], uint8_t lowest[Y][X])
{
        uint8_t x, y;
        uint64_t count = 0;
        for (y = 0; y < Y; ++y)
                for (x = 0; x < X; ++x)
                        if (lowest[y][x])
                                count += 1 + table[y][x];

        return count;
}

void part1(void)
{
        char *input, *begin;
        uint64_t answer = 0;
        uint8_t table[Y][X]  = { 0 };
        uint8_t lowest[Y][X] = { 0 };

        input = read_input(INPUT_FILE);
        begin = input;

        init_table(input, table);
        mark_lowest(table, lowest);
        answer = count_lowest(table, lowest);
        // print_lowest(table, lowest);

        free(begin);

        printf("Part 1: %lu\n", answer);
}

int64_t DFS(uint8_t table[Y][X], uint8_t visited[Y][X], uint8_t y, uint8_t x)
{
        if (visited[y][x])
                return 0;

        int64_t cnt   = 1;
        visited[y][x] = 1;

        cnt += y > 0     && table[y-1][x] < 9 ? DFS(table, visited, y-1, x) : 0;
        cnt += x < X - 1 && table[y][x+1] < 9 ? DFS(table, visited, y, x+1) : 0;
        cnt += y < Y - 1 && table[y+1][x] < 9 ? DFS(table, visited, y+1, x) : 0;
        cnt += x > 0     && table[y][x-1] < 9 ? DFS(table, visited, y, x-1) : 0;

        return cnt;
}

int cmp(const void *a, const void *b)
{
        return *(int64_t *)b - *(int64_t *)a;
}

void part2(void)
{
        char *input, *begin;
        uint8_t x, y, i;
        uint64_t answer = 0;
        uint8_t table[Y][X]      = { 0 };
        uint8_t lowest[Y][X]     = { 0 };
        uint8_t visited[Y][X]    = { 0 };
        int64_t basins[BASINS_N] = { 0 };

        input = read_input(INPUT_FILE);
        begin = input;

        init_table(input,   table);
        mark_lowest(table,  lowest);
        print_lowest(table, lowest);

        i = 0;
        for (y = 0; y < Y; ++y)
                for (x = 0; x < X; ++x)
                        if (lowest[y][x])
                                basins[i++] = DFS(table, visited, y, x);

        qsort(basins, BASINS_N, sizeof(int64_t), cmp);
        answer = basins[0] * basins[1] * basins[2];

        free(begin);
        printf("Part 2: %ld\n", answer);
}

int main(void)
{
        part1();
        part2();
        return 0;
}
