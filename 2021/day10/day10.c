// https://adventofcode.com/2021/day/10

#include <stdio.h>
#include <string.h>
#include "../common.c"

#if 0
#define INPUT_FILE "sample.txt"
#else
#define INPUT_FILE "input.txt"
#endif

#define VERBOSE  1
#define BRACKETS 4
#define MAX_LINE 128
#define MAX_ROWS 128

uint32_t points[BRACKETS]  = { 3, 57, 1197, 25137 };
char     opening[BRACKETS] = "([{<";
char     closing[BRACKETS] = ")]}>";

int cmp(const void *a, const void *b)
{
        if (*(uint64_t *)a > *(uint64_t *)b) {
                return 1;
        }

        return -1;
}

void print_expected(char *input, char *stack, uint32_t input_i, uint32_t stack_i)
{
        printf("Expected `%c`, but found `%c` instead\n", stack[stack_i], input[input_i]);
        printf("Stack: %s\n", stack);
        printf("       %*s^\n", stack_i, "");
        printf("Input: %s\n", input);
        printf("       %*s^\n\n", input_i, " ");
}

int main(void)
{
        char *input, *begin, *next, stack[MAX_LINE];
        uint32_t ans, sz, i, j, ans2_i;
        uint64_t ans2[MAX_ROWS] = { 0 };

        input = read_input(INPUT_FILE);
        begin = input;
        ans   = ans2_i = 0;

        while (input) {
                memset(stack, 0, MAX_LINE);
                sz = 0;
                next = chop(input, '\n');
                for (i = 0; input[i] != '\0'; ++i)
                        for (j = 0; j < BRACKETS; ++j) {
                                if (input[i] == opening[j]) {
                                        stack[sz++] = input[i];
                                        break;
                                }
                                if (input[i] == closing[j]) {
                                        if (stack[sz-1] == opening[j]) {
                                                stack[sz---1] = '\0';
                                                break;
                                        } else {
                                                ans += points[j];
                                                if (VERBOSE)
                                                        print_expected(input, stack, i, sz-1);
                                                sz = 0;
                                                goto end;
                                        }
                                }
                        }

                // Part 2://////////////////////////////////////////////////////////////////////////
                if (sz) {                                                                         //
                        while (sz--) {                                                            //
                                ans2[ans2_i] *= 5;                                                //
                                for (i = 0; i < BRACKETS; ++i) {                                  //
                                        if (stack[sz] == opening[i]) {                            //
                                                ans2[ans2_i] += i+1;                              //
                                        }                                                         //
                                }                                                                 //
                        }                                                                         //
                        ++ans2_i;                                                                 //
                }                                                                                 //
                ////////////////////////////////////////////////////////////////////////////////////
                end:                                                                              //
                input = next;                                                                     //
        }                                                                                         //
                                                                                                  //
        ////////////////////////////////////////////////////////////////////////////////////////////
        qsort(ans2, ans2_i, sizeof(int64_t), cmp);                                                //
        ////////////////////////////////////////////////////////////////////////////////////////////

        free(begin);
        printf("Part 1: %u\n", ans );
        printf("Part 2: %lu\n", ans2[ans2_i/2]);

        return 0;
}

