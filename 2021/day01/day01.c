// https://adventofcode.com/2021/day/1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if 0
#define INPUT_FILE "sample.txt"
#else
#define INPUT_FILE "input.txt"
#endif
#define DELIM '\n'

char *read_input(char *input_file)
{
        FILE *f = fopen(input_file, "r");
        char *file_str = NULL;
        unsigned len = 0;

        fseek(f, 0L, SEEK_END);
        len = ftell(f);
        if (f == NULL || len == 0) return NULL;
        rewind(f);
        file_str = calloc(1, len + 1);
        if (file_str == NULL) return NULL;

        while(fread(file_str, len, 1, f) == len);
        fclose(f);

        return file_str;
}

unsigned *parse_str(unsigned len, char *str, char delim)
{
        unsigned *num_arr = calloc(len, sizeof(unsigned));
        if (num_arr == NULL) return NULL;

        for (unsigned i = 0; i < len; ++i) {
                num_arr[i] = atoi(str);
                str = strchr(str, delim) + 1;
        }

        return num_arr;
}

unsigned part1(void)
{
        char *file_str = read_input(INPUT_FILE);
        unsigned len, result, prev, i;
        unsigned *nums_arr = NULL;
        len = result = i = 0;
        prev = ~0;

        while (file_str[i] != '\0') {
                len += file_str[i++] == '\n';
        }

        if ((nums_arr = parse_str(len, file_str, DELIM)) == NULL) { return 0; }

        for (i = 0; i < len; ++i) {
                result += nums_arr[i] > prev;
                prev = nums_arr[i];
        }

        free(nums_arr);
        free(file_str);

        return result;
}


unsigned part2(void)
{
        char *file_str = read_input(INPUT_FILE);
        unsigned len, prev, window, result, i;
        unsigned *nums_arr = NULL;
        len = window = result = i =  0;
        prev = ~0;

        while (file_str[i] != '\0') {
                len += file_str[i++] == '\n';
        }

        if ((nums_arr = parse_str(len, file_str, DELIM)) == NULL) { return 0; }

        for (i = 2; i < len; ++i) {
                window = nums_arr[i] + nums_arr[i-1] + nums_arr[i-2];
                result += window > prev;
                prev = window;
        }

        free(nums_arr);
        free(file_str);

        return result;
}

int main(void)
{
        printf("Part 1: %u\n", part1());
        printf("Part 2: %u\n", part2());

        return 0;
}
