// https://adventofcode.com/2021/day/4

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <ctype.h>
#include <stdbool.h>
#include "../common.c"

#if 0
#define INPUT_FILE "sample.txt"
#define TABLE_N 3
#define INPUT_LEN 27
#else
#define INPUT_FILE "input.txt"
#define TABLE_N 100
#define INPUT_LEN 100
#endif

#define MASK 128
#define DIM 5

bool won[TABLE_N];
uint8_t wins;

void parse_tables(uint8_t tables[TABLE_N][DIM][DIM], char *str)
{
        for (uint8_t table = 0; table < TABLE_N; ++table) {
                while(isspace(*str)) { str++; }
                for (uint8_t y = 0; y < DIM; ++y) {
                        for (uint8_t x = 0; x < DIM; ++x) {
                                while(isspace(*str)) { str++; }
                                tables[table][y][x] = atoi(str);
                                while(isdigit(*str)) { str++; }
                        }
                }
        }
}

void PrintTable(uint8_t table[DIM][DIM])
{
        for (uint8_t y = 0; y < DIM; ++y) {
                for (uint8_t x = 0; x < DIM; ++x) {
                        if (table[y][x] >= MASK)
                                printf("(%2u)", table[y][x] -  MASK);
                        else
                                printf(" %2u ", table[y][x]);
                }
                printf("\n");
        }
        printf("\n");
}

void PrintTables(uint8_t tables[TABLE_N][DIM][DIM])
{
        for (uint8_t table = 0; table < TABLE_N; ++table) {
                PrintTable(tables[table]);
        }
}

void parse_input(char *str, uint8_t *input_arr)
{
        char *tok = strtok(str, ",");
        for (uint8_t i = 0; i < INPUT_LEN; ++i) {
                input_arr[i] = atoi(tok);
                tok = strtok(NULL, ",");
        }
}

void AddToTable(uint8_t table[DIM][DIM], uint8_t num)
{
        for (uint8_t y = 0; y < DIM; ++y) {
                for (uint8_t x = 0; x < DIM; ++x) {
                        if (table[y][x] == num) {
                                table[y][x] += MASK;
                        }
                }
        }
}

void add_num(uint8_t table[TABLE_N][DIM][DIM], uint8_t num)
{
        for (uint8_t i = 0; i < TABLE_N; ++i) {
                if (won[i] == 0) {
                        AddToTable(table[i], num);
                }
        }
}

bool check_table(uint8_t table[DIM][DIM])
{
        for (uint8_t y = 0; y < DIM; ++y) {
                uint8_t row = 0;
                uint8_t col = 0;
                for (uint8_t x = 0; x < DIM; ++x) {
                        row += table[y][x] >> 7;
                        col += table[x][y] >> 7;
                }
                if (row == DIM || col == DIM) {
                        return true;
                }
        }
        return false;
}

int check_winners(uint8_t tables[TABLE_N][DIM][DIM])
{
        int result = -1;

        for (int i = 0; i < TABLE_N; ++i) {
                if (won[i] == 0) {
                        if (check_table(tables[i])) {
                                won[i] = 1;
                                wins++;
                                result = i;
                        }
                }
        }
        return result;
}

uint32_t Count(uint8_t table[DIM][DIM])
{
        uint32_t sum = 0;
        for (uint8_t y = 0; y < DIM; ++y) {
                for (uint8_t x = 0; x < DIM; ++x) {
                        if (table[y][x] < MASK) {
                                sum += table[y][x];
                        }
                }
        }
        return sum;
}

int part1(void)
{
        memset(won, 0, sizeof(bool) * TABLE_N);
        wins = 0;
        char *input = read_input(INPUT_FILE);
        char *table_str = strchr(input, '\n');
        uint8_t tables[TABLE_N][DIM][DIM];
        uint8_t input_arr[INPUT_LEN];
        int winner = -1;
        *table_str = 0;
        table_str++;
        parse_tables(tables, table_str);
        parse_input(input, input_arr);

        for (uint8_t i = 0; i < INPUT_LEN; ++i) {
                add_num(tables, input_arr[i]);
                if (i >= DIM - 1) {
                        winner = check_winners(tables);
                        if (winner >= 0) {
                                uint32_t sum = Count(tables[winner]);
                                printf("Part 1: %u\n", sum * input_arr[i]);
                                break;
                        }
                }
        }
        free(input);
        if (winner < 0) {
                return winner;
        }
        return 0;
}

int part2(void)
{
        memset(won, 0, sizeof(bool) * TABLE_N);
        wins = 0;
        char *input = read_input(INPUT_FILE);
        char *table_str = strchr(input, '\n');
        uint8_t tables[TABLE_N][DIM][DIM];
        uint8_t input_arr[INPUT_LEN];
        int winner = -1;
        *table_str = 0;
        table_str++;
        parse_tables(tables, table_str);
        parse_input(input, input_arr);

        for (uint8_t i = 0; i < INPUT_LEN; ++i) {
                add_num(tables, input_arr[i]);
                if (i >= DIM - 1) {
                        winner = check_winners(tables);
                        if ((winner >= 0) && (wins == TABLE_N)) {
                                uint32_t sum = Count(tables[winner]);
                                printf("Part 2: %u\n", sum * input_arr[i]);
                                break;
                        }
                }
        }
        free(input);
        if (winner < 0) {
                return winner;
        }
        return 0;
}

int main(void)
{
        part1();
        part2();
        return 0;
}
