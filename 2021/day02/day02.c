// https://adventofcode.com/2021/day/2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../common.c"
#include <ctype.h>

#if 0
#define INPUT_FILE "sample.txt"
#else
#define INPUT_FILE "input.txt"
#endif

#define X 0
#define Y 1

char *chop(char *str)
{
        size_t len = strlen(str), i = 0;
        char *next = NULL;
        while (i + 1 < len) {
                if (isspace(str[i])) {
                        next = &str[i] + 1;
                        break;
                }
                i++;
        }

        return next;
}

unsigned
part1(void)
{
        unsigned pos[] = { 0, 0 };
        char direction, amount;
        char *input = read_input(INPUT_FILE);
        char *begin = input;

        while (input != NULL) {
                direction = *input;
                input = chop(input);
                amount = *input;
                input = chop(input);
                switch(direction) {
                        case 'f':
                                pos[X] += amount - '0';
                                break;
                        case 'u':
                                pos[Y] -= amount - '0';
                                break;
                        case 'd':
                                pos[Y] += amount - '0';
                                break;
                        default:
                                break;
                }
        }

        free(begin);
        return pos[X] * pos[Y];
}


unsigned
part2(void)
{
        unsigned pos[] = { 0, 0 };
        unsigned aim = 0;
        char cmd = 0, x = 0;
        char *input = read_input(INPUT_FILE);
        char *begin = input;

        while (input != NULL) {
                cmd = *input;
                input = chop(input);
                x = *input;
                input = chop(input);
                switch(cmd) {
                        case 'f':
                                pos[X] += x - '0';
                                pos[Y] += aim * (x - '0');
                                break;
                        case 'u':
                                aim -= x - '0';
                                break;
                        case 'd':
                                aim += x - '0';
                                break;
                        default:
                                break;
                }
        }

        free(begin);
        return pos[X] * pos[Y];
}

int main(void)
{
        printf("Part 1: %u\n", part1());
        printf("Part 2: %u\n", part2());
}
