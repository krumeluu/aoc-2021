// https://adventofcode.com/2021/day/3

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include "../common.c"

#if 0
#define INPUT_FILE "sample.txt"
#define BYTE_LEN 5
#define INPUT_CAP 12
#else
#define INPUT_FILE "input.txt"
#define BYTE_LEN 12
#define INPUT_CAP 1024
#endif

unsigned bits[BYTE_LEN] = { 0 };

unsigned part1(void)
{
        char *input, *begin;
        unsigned i, j, gamma, epsilon, ln_cnt;
        gamma = epsilon = 0;
        input = read_input(INPUT_FILE);
        begin = input;
        ln_cnt = line_cnt(input, strlen(input));

        for (i = 0; i < ln_cnt; ++i) {
                for (j = 0; j < BYTE_LEN; ++j) {
                        bits[j] += *input++ == '1';
                }
                input++;
        }

        for (i = 0, j = BYTE_LEN - 1; i < BYTE_LEN; ++i, --j) {
                bits[i] = bits[i] >= ln_cnt / 2;
                gamma |= bits[i] << j;
        }
        epsilon = ~gamma ^ (~0U << BYTE_LEN);

        free(begin);
        return gamma * epsilon;
}

void print_bytes(char **bytes, unsigned n)
{
        for (unsigned i = 0; i < n; ++i) {
                printf("%02u: %s\n", i, bytes[i]);
        }
        puts("");
}

void cross_bytes(char **bytes, bool *crossed, unsigned ln_cnt, unsigned byte)
{
        for (unsigned i = 0; i < ln_cnt; ++i) {
                if (crossed[i])
                        bytes[i][byte] = '*';
        }
}


unsigned part2(void)
{
        char *input = read_input(INPUT_FILE);
        char *begin = input;
        char *bytes[INPUT_CAP];
        char most_common, least_common;
        int tally_o, tally_co;
        unsigned i, j, k, ln_cnt, o_left, co_left, oxygen, co2;
        bool crossed_O[INPUT_CAP]  = { 0 };
        bool crossed_CO[INPUT_CAP] = { 0 };

        ln_cnt  = line_cnt(input, strlen(input));
        o_left  = ln_cnt;
        co_left = ln_cnt;

        for (j = 0; j < ln_cnt; ++j, input += BYTE_LEN + 1) {
                input[BYTE_LEN] = 0;
                bytes[j] = input;
        }

        for (i = 0; i < BYTE_LEN && (o_left > 1 || co_left > 1); ++i) {
                tally_o  = 0;
                tally_co = 0;
                for (j = 0; j < ln_cnt; ++j) {
                        if (!crossed_O[j])
                                tally_o  += bytes[j][i] == '1' ? 1 : -1;
                        if (!crossed_CO[j])
                                tally_co += bytes[j][i] == '1' ? 1 : -1;
                }

                most_common  = tally_o  >= 0 ? '1' : '0';
                least_common = tally_co >= 0 ? '0' : '1';

                for (j = 0; j < ln_cnt; ++j) {
                        if (o_left > 1) {
                                if (!crossed_O[j] && bytes[j][i] != most_common) {
                                        crossed_O[j] = true;
                                        o_left--;
                                }
                        }
                        if (co_left > 1) {
                                if (!crossed_CO[j] && bytes[j][i] != least_common) {
                                        crossed_CO[j] = true;
                                        co_left--;
                                }
                        }
                }
        }

        /* print_bytes(bytes, ln_cnt); */
        i = 0;
        j = 0;
        while (crossed_O[i])  i++;
        while (crossed_CO[j]) j++;
        oxygen = 0;
        co2    = 0;

        for (k = 0; k < BYTE_LEN; ++k ) {
                oxygen += (bytes[i][BYTE_LEN - 1 - k] - '0') << k;
                co2    += (bytes[j][BYTE_LEN - 1 - k] - '0') << k;
        }

        free(begin);
        return oxygen * co2;
}

int main(void)
{
        printf("Part 1: %u\n", part1());
        printf("Part 2: %u\n", part2());

        return 0;
}
