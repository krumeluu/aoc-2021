#!/bin/bash

if [ $# -ne 1 ]; then
        echo "Please give executable as an argument"
        echo "Exiting..."
        exit 1
fi

valgrind -v --leak-check=full --track-origins=yes -q "$1" 2> valgrind.log
