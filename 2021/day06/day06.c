// https://adventofcode.com/2021/day/6

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include "../common.c"

#if 0
#define INPUT_FILE "sample.txt"
#else
#define INPUT_FILE "input.txt"
#endif


void parse_input(char *input, unsigned long fish[8])
{
        if (input == NULL) return;

        while (1) {
                fish[atoi(input)]++;
                input = strchr(input, ',');
                if (input++ == NULL) {
                        break;
                }
        }
}


unsigned long
part1(void)
{
        unsigned timer_vals = 9;                        // nr. of different values for inner timer
        unsigned long fish[9] = { 0 };
        unsigned long sum = 0;                          // init sum of timer values to 0
        unsigned days = 80;                             // days to count
        unsigned head_start = 2;                        // how much faster can old fish reproduce than newborn
        unsigned long tmp;
        char *input = read_input(INPUT_FILE);
        parse_input(input, fish);

        for (unsigned i = 0; i < timer_vals; ++i) {
                sum += fish[i];
        }

        for (unsigned day = 0; day < days; ++day) {
                tmp = fish[0];
                /* for (unsigned i = 0; i < timer_vals; ++i) { */
                /*         printf("%4lu ", fish[i]); */
                /* } */
                /* printf("\nSum: %lu\n", sum); */
                for (unsigned i = 1; i < timer_vals; ++i) {
                        fish[i-1] = fish[i];
                }
                fish[timer_vals - head_start - 1] += tmp;
                fish[timer_vals - 1] = tmp;
                sum += tmp;
        }

        free(input);
        return sum;
}


unsigned long
part2(void)
{
        unsigned timer_vals = 9;                        // nr. of different values for inner timer
        unsigned long fish[9] = { 0 };
        unsigned long sum = 0;                          // init sum of timer values to 0
        unsigned days = 256;                            // days to count
        unsigned head_start = 2;                        // how much faster can old fish reproduce than newborn
        unsigned long tmp;
        char *input = read_input(INPUT_FILE);
        parse_input(input, fish);

        for (unsigned i = 0; i < timer_vals; ++i) {
                sum += fish[i];
        }

        for (unsigned day = 0; day < days; ++day) {
                tmp = fish[0];
                /* for (unsigned i = 0; i < timer_vals; ++i) { */
                /*         printf("%4lu ", fish[i]); */
                /* } */
                /* printf("\nSum: %lu\n", sum); */
                for (unsigned i = 1; i < timer_vals; ++i) {
                        fish[i-1] = fish[i];
                }
                fish[timer_vals - head_start - 1] += tmp;
                fish[timer_vals - 1] = tmp;
                sum += tmp;
        }

        free(input);
        return sum;
}

int main(void)
{
        printf("Part 1: %lu\n", part1());
        printf("Part 2: %lu\n", part2());

        return 0;
}
