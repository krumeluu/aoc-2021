#include <stdlib.h>
#include <stdint.h>

char *read_input(char *input_file)
{
        FILE *f = fopen(input_file, "r");
        char *file_str = NULL;
        size_t len = 0;

        fseek(f, 0L, SEEK_END);
        len = ftell(f);
        if (f == NULL || len == 0) return NULL;
        rewind(f);
        file_str = calloc(1, len + 1);
        if (file_str == NULL) return NULL;

        while(fread(file_str, len, 1, f) == len);
        fclose(f);

        return file_str;
}

unsigned line_cnt(char *str, size_t len)
{
        unsigned n = 0, i = 0;
        while (i < len) n += str[i++] == '\n';
        return n;
}


// Null terminate by delimiter and return pointer to next segment
// If end of string is encountered, return NULL
char *chop(char *input, char delim)
{
        while (*input != '\0') {
                if (*input == delim) {
                        *input = '\0';
                        return input + 1;
                }
                ++input;
        }

        return NULL;
}


// Print a byte
void print_byte(uint8_t byte)
{
        for (uint8_t mask = 0x80; mask > 0; mask >>= 1) {
                printf("%u", (byte & mask) && mask);
        }
}
