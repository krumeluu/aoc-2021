// https://adventofcode.com/2021/day/7

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../common.c"

#if 0
#define INPUT_FILE "sample.txt"
#else
#define INPUT_FILE "input.txt"
#endif

#define DELIM ','

unsigned *parse_input(char *input, unsigned n, char delim)
{
        unsigned *crabs = calloc(n, sizeof(unsigned));
        unsigned i = 0;
        while (input != NULL) {
                if (*input == delim) {
                        input++;
                }
                crabs[i++] = atoi(input);
                input = strchr(input, delim);
        }

        return crabs;
}

unsigned count_input(char *input, char delim)
{
        if (input == NULL) return 0;
        unsigned n = 1;
        while (*input != '\0') {
                n += *input++ == delim;
        }
        return n;
}

unsigned find_max(unsigned crabs[], unsigned n)
{
        unsigned max = 0;
        while (n--) {
                if (crabs[n] > max) {
                        max = crabs[n];
                }
        }
        return max;
}

void part1(void)
{
        unsigned f, c, min_f;
        char     *input  = read_input(INPUT_FILE);
        unsigned n_input = count_input(input, DELIM);
        unsigned *crabs  = parse_input(input, n_input, DELIM);
        unsigned max     = find_max(crabs, n_input);
        unsigned *fuel   = calloc(max + 1, sizeof(unsigned));

        for (f = 0; f < max; ++f) {
                for (c = 0; c < n_input; ++c) {
                        fuel[f] += f > crabs[c] ? f - crabs[c] : crabs[c] - f;
                }
        }

        for (f = 0, min_f = ~ 0; f < max; ++f) {
                if (fuel[f] < min_f) {
                        min_f = fuel[f];
                }
        }

        printf("Part 1: %u\n", min_f);

        free(crabs);
        free(input);
        free(fuel) ;
}

void part2(void)
{
        unsigned f, c, min_f, diff;
        char     *input  = read_input(INPUT_FILE);
        unsigned n_input = count_input(input, DELIM);
        unsigned *crabs  = parse_input(input, n_input, DELIM);
        unsigned max     = find_max(crabs, n_input);
        unsigned *fuel   = calloc(max + 1, sizeof(unsigned));

        for (f = 0; f < max; ++f) {
                for (c = 0; c < n_input; ++c) {
                        diff= f > crabs[c] ? f - crabs[c] : crabs[c] - f;
                        fuel[f] += diff * (diff + 1) / 2;
                }
        }

        for (f = 0, min_f = ~ 0; f < max; ++f) {
                if (fuel[f] < min_f) {
                        min_f = fuel[f];
                }
        }

        printf("Part 2: %u\n", min_f);

        free(crabs);
        free(input);
        free(fuel) ;

}

int main(void)
{
        part1();
        part2();
        return 0;
}
