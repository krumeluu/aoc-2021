#!/bin/sh

# Count number of existing folders
str=$(ls -l | grep ^d | wc -l)

# Add 1
n=$((str + 1))

# Pad with 0
printf -v str "%02d" $n

# Concatenate "day" and number of dirs
str=day$str

# Make and enter new directory
mkdir -v $str
cd $str

# Copy templates
echo "// https://adventofcode.com/2021/day/$n" > $str.c
cat ../template.c >> $str.c

# # Get input for puzzle
# wget https://adventofcode.com/2021/day/$n/input -O input.txt
