package day07

import (
	. "aoc/utils"
	"math"
	"strconv"
)

func Part2(filepath string) int {
	input := GetInput(filepath)
	strArr := GetRowsByCols(input, `(:)? `)
	arr := StrsAsInts2d(strArr)
	sum := 0

	for i := range arr {
		if validEq(arr[i][0], 0, arr[i][1:]) {
			sum += arr[i][0]
		}
	}

	return sum
}

func validEq(n, sum int, operands []int) bool {
	if n == sum && len(operands) == 0 {
		return true
	} else if len(operands) == 0 {
		return false
	}
	addSum := sum + operands[0]
	mulSum := sum * operands[0]
	catSum := cat(sum, operands[0])
	return validEq(n, addSum, operands[1:]) ||
	       validEq(n, mulSum, operands[1:]) ||
	       validEq(n, catSum, operands[1:])
}

func cat(a, b int) int {
	return int(math.Pow(10.0, float64(len(strconv.Itoa(b))))) * a + b
}
