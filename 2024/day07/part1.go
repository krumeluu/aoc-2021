package day07

import (
	. "aoc/utils"
)

func Part1(filepath string) int {
	input := GetInput(filepath)
	strArr := GetRowsByCols(input, `(:)? `)
	arr := StrsAsInts2d(strArr)
	sum := 0

	for i := range arr {
		if isValid(arr[i][0], 0, arr[i][1:]) {
			sum += arr[i][0]
		}
	}

	return sum
}

func isValid(n, sum int, operands []int) bool {
	if n == sum && len(operands) == 0 {
		return true
	} else if len(operands) == 0 {
		return false
	}
	addSum := sum + operands[0]
	mulSum := sum * operands[0]
	return isValid(n, addSum, operands[1:]) ||
	       isValid(n, mulSum, operands[1:])
}
