package day03

import (
	"aoc/utils"
	"regexp"
	"strconv"
	"strings"
)

func Part1(filepath string) int {
	var product int
	input := utils.GetInput(filepath)
	re := regexp.MustCompile(`mul\(\d{1,3},\d{1,3}\)`)
	matches := re.FindAllString(input, -1)

	for _, m := range matches {
		left, right, _ := strings.Cut(m[4:len(m)-1], ",")
		a, _ := strconv.Atoi(left)
		b, _ := strconv.Atoi(right)
		product += a * b
	}
	return product
}
