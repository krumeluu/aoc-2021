package day03

import (
	. "aoc/utils"
	"regexp"
	"strconv"
	"strings"
)

func Part2(filepath string) int {
	var product int
	input := GetInput(filepath)
	re := regexp.MustCompile(`mul\(\d{1,3},\d{1,3}\)|do\(\)|don't\(\)`)
	matches := re.FindAllString(input, -1)
	do := true

	for _, m := range matches {
		if m == "do()" || m == "don't()" || !do {
			do = m == "do()"
		} else {
			left, right, _ := strings.Cut(m[4:len(m)-1], ",")
			a, _ := strconv.Atoi(left)
			b, _ := strconv.Atoi(right)
			product += a * b
		}
	}
	return product
}
