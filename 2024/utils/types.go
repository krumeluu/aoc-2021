package utils

type Coord struct {
	Y int;
	X int;
}

type Pos struct {
	Y   int;
	X   int;
	Dir int;
}
