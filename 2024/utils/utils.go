package utils

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

func NextCoord(pos, dir Coord) Coord {
	return Coord {
		Y: pos.Y + dir.Y,
		X: pos.X + dir.X,
	}
}

// Split integer at the middle and return each half
func SplitInt(n int) (left, right int) {
	half := IntLen(n) / 2
	left = n / (IntPow(10, half))
	right = n % IntPow(10, half)
	return
}

// Raise n to power of e
func IntPow(n, e int) int {
	product := 1
	for range e {
		product *= n
	}
	return product
}

// Count the digit length of int
func IntLen(n int) int {
	if n == 0 {
		return 1
	}
	count := 0
	for n != 0 {
		n /= 10
		count++
	}
	return count
}

// Remove element of arr at i
func RemoveAtIndex[T any](a []T, i int) []T {
	return append(a[:i], a[i+1:]...)
}

// Remove element of arr at i
func FilterIndex[T any](slice []T, i int) []T {
	trimmed := append([]T(nil), slice[:i]...)
	return append(trimmed, slice[i+1:]...)
}

// Split lines into strings by delim
func LinesToStrArr(lines []string, delim string) [][]string {
	strs := [][]string{}
	re := regexp.MustCompile(delim)
	for _, line := range lines {
		lineArr := re.Split(line, -1)
		strs = append(strs, lineArr)
	}
	return strs
}

// Check if values are unique
func UniqueInts(slice []int) bool {
	sort.Ints(slice)
	for i := 0; i < len(slice)-1; i++ {
		if slice[i] == slice[i+1] {
			return false
		}
	}
	return true
}

// Check that indices are in bounds of the slice
func InBounds[T any](arr [][]T, y, x int) bool {
	return y >= 0 && x >= 0 && y < len(arr) && x < len(arr[y])
}

// Get 2d string slice where words are cut by delim
func GetRowsByCols(input, delim string) [][]string {
	lines := GetLines(input)
	arr := LinesToStrArr(lines, delim)
	return arr
}

// Split by newlines
func GetLines(input string) []string {
	return strings.Split(input, "\n")
}

// Get 2d slice transposed
func GetColsByRows(input, delim string) [][]string {
	inputArr := GetRowsByCols(input, delim)
	return TransposeMatrix(inputArr)
}

// Produce an empty array of any type by dimensions x * y
func GetEmpty2dArr[T any](y, x int) [][]T {
	arr := make([][]T, y)
	for i := range arr {
		arr[i] = make([]T, x)
	}
	return arr
}

// Check equality with ambiguous type
func Equal(a, b interface{}) bool {
	return fmt.Sprint(a) == fmt.Sprint(b)
}

// Delete row from index i of 2d slice
func DeleteRow[T any](arr [][]T, i int) [][]T {
	return append(arr[:i], arr[i+1:]...)
}

// Don't split by newline, only by empty new line
func SplitByEmptyNewline(str string) []string {
	return regexp.
		MustCompile(`\n\s*\n`).
		Split(str, -1)
}

// Parse multiline input into 2d array of runes
func ParseAsRunes2d(input string) [][]rune {
	lines := strings.Split(input, "\n")
	runes := make([][]rune, len(lines))
	for i, line := range lines {
		runes[i] = []rune(line)
	}
	return runes
}

// Convert an array of runes into ints
func RunesAsInts(a []rune) []int {
	n := make([]int, len(a))
	for i, r := range a {
		n[i] = int(r) - '0'
	}
	return n
}

// Convert 2d array of runes into ints
func RunesAsInts2d(a [][]rune) [][]int {
	n := make([][]int, len(a))
	for i, r := range a {
		n[i] = RunesAsInts(r)
	}
	return n
}

// Convert an array of strings into ints
func StrsAsInts(row []string) []int {
	var ints = []int{}
	for _, element := range row {
		asInt, err := strconv.Atoi(element)
		check(err)
		ints = append(ints, asInt)
	}
	return ints
}

// Convert a 2d array of strings into ints
func StrsAsInts2d(strArr [][]string) [][]int {
	var intArr = [][]int{}
	for _, strs := range strArr {
		ints := StrsAsInts(strs)
		intArr = append(intArr, ints)
	}
	return intArr
}

// Copy a 2d array of any type
func Copy2dSlice[T any](source [][]T) [][]T {
	dup := make([][]T, len(source))
	for i := range source {
		dup[i] = make([]T, len(source[i]))
		copy(dup[i], source[i])
	}
	return dup
}

// Return coordinates of 'find'
func GetCoords[T any](arr [][]T, find T) [][]int {
	coords := [][]int{}
	for y, row := range arr {
		for x, r := range row {
			if Equal(r, find) {
				coords = append(coords, []int{y, x})
			}
		}
	}
	return coords
}

// Check that numbers are withing the margin of error
func WithinMargin(a, b, margin int) bool {
	diff := int(math.Abs(float64(a) - float64(b)))
	return diff <= margin
}

// Transpose a matrix
func TransposeMatrix[T any](matrix [][]T) [][]T {

	rows := len(matrix)
	cols := len(matrix[0])
	new := make([][]T, cols)

	for i := 0; i < cols; i++ {
		new[i] = make([]T, rows)
		for j := 0; j < rows; j++ {
			new[i][j] = matrix[j][i]
		}
	}
	return new
}

// Sort coordinates by Y coordinate
func SortCoords(a, b Coord) int {
	if a.Y < b.Y {
		return -1
	}
	if a.Y > b.Y {
		return 1
	}
	if a.X < b.X {
		return -1
	}
	if a.X > b.X {
		return 1
	}
	return 0
}

// Sort coordinates by X coordinate
func SortCoordsByX(a, b Coord) int {
	if a.X < b.X {
		return -1
	}
	if a.X > b.X {
		return 1
	}
	if a.Y < b.Y {
		return -1
	}
	if a.Y > b.Y {
		return 1
	}
	return 0
}

// Read a file into a string
func GetInput(filename string) string {
	file, err := os.ReadFile(filename)
	check(err)
	return strings.TrimSpace(string(file))
}

// Halt output and continue with Return
func Step() (string, bool) {

	in := bufio.NewReader(os.Stdin)
	input, err := in.ReadString('\n')
	if err != nil {
		return "", false
	}
	input = strings.TrimSpace(input)
	return input, true
}

// Zero-based to one-based
func OneBasedCoord(pos Coord) Coord {
	return Coord {
		Y: pos.Y + 1,
		X: pos.X + 1,
	}
}

// Flip Y axis of a coordinate
func FlipY[T any](pos Coord, arr []T) Coord {
	pos.Y = len(arr) - pos.Y - 1
	return pos
}

// Y-flipped one-based coordinate
func HumanCoord[T any](c Coord, arr []T) Coord {
	return OneBasedCoord(FlipY(c, arr))
}

// Clear screen
func ClearScreen() {
	fmt.Print("\033[H\033[2J")
}

// Check error and panic
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Print a 2d slice of runes
func Print2dSlice(s [][]rune) {
	for _, row := range s {
		for _, r := range row {
			fmt.Printf("%c", r)
		}
		println()
	}
}
