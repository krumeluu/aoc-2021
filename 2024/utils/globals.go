package utils

var Dirs4 = []Coord {
	{Y:  0, X:  1}, // East
	{Y:  1, X:  0}, // South
	{Y:  0, X: -1}, // West
	{Y: -1, X:  0}, // North
}

var Corners = []Coord{
	{Y:  1, X:  1}, // SE
	{Y:  1, X: -1}, // SW
	{Y: -1, X: -1}, // NW
	{Y: -1, X:  1}, // NE
}

var Dirs8 = []Coord {
	{Y:  0, X:  1}, // E
	{Y:  1, X:  1}, // SE
	{Y:  1, X:  0}, // S
	{Y:  1, X: -1}, // SW
	{Y:  0, X: -1}, // W
	{Y: -1, X: -1}, // NW
	{Y: -1, X:  0}, // N
	{Y: -1, X:  1}, // NE
}
