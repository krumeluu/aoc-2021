package day01

import (
	. "aoc/utils"
)

func Part2(filename string) int {

	input := GetInput(filename)
	lists := StrsAsInts2d(GetColsByRows(input, `\s+`))
	listA := lists[0]
	listB := lists[1]

	var score int

	for i := 0; i < len(listA); i++ {
		for j := 0; j < len(listB); j++ {
			if listA[i] == listB[j] {
				score += listA[i]
			}
		}
	}

	return score
}
