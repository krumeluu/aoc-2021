package day01

import (
	. "aoc/utils"
	"math"
	"sort"
)

func Part1(filename string) int {

	input := GetInput(filename)
	lists := StrsAsInts2d(GetColsByRows(input, `\s+`))
	list_a := (lists[0])
	list_b := lists[1]

	sort.Ints(list_a)
	sort.Ints(list_b)

	var diffs int

	for i := 0; i < len(list_a); i++ {
		a, b := list_a[i], list_b[i]
		diffs += int(math.Abs(float64(a) - float64(b)))
	}

	return diffs
}
