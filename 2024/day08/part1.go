package day08

import (
	. "aoc/utils"
)

type Char struct {
	r rune
	y int
	x int
}

func Part1(filepath string) int {
	input := ParseAsRunes2d(GetInput(filepath))
	nodes := getNodes(input)
	antinodes := GetEmpty2dArr[bool](len(input), len(input[0]))

	for _, n := range nodes {
		for y, row := range input {
			for x, r := range row {
				if n.y == y && n.x == x { continue }
				if n.r == r {
					refX, refY := mirrorCoord(n.y, n.x, y, x)
					if InBounds(input, refY, refX) {
						antinodes[refY][refX] = true
					}
					refY, refX = mirrorCoord(x, y, n.x, n.y)
					if InBounds(input, refY, refX) {
						antinodes[refY][refX] = true
					}
				}
			}
		}
	}
	return countTrue2d(antinodes)
}

func mirrorCoord(pointY, pointX, sourceY, sourceX int) (int, int) {
	diffY := sourceY - pointY
	diffX := sourceX - pointX
	return pointY - diffY, pointX - diffX
}

func countTrue2d(a [][]bool) int {
	sum := 0
	for y := range a {
		for x := range a[y] {
			if a[y][x] {
				sum++
			}
		}
	}
	return sum
}

func getNodes(s [][]rune) []Char {
	runes := []Char{}
	for y, row := range s {
		for x, r := range row {
			if r >= '0' && r <= '9' || r >= 'A' && r <= 'Z' || r >= 'a' && r <= 'z' {
				runes = append(runes, Char{r, y, x})
			} 
		}
	}
	return runes
}
