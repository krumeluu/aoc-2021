package day08

import (
	. "aoc/utils"
)

func Part2(filepath string) int {
	input := ParseAsRunes2d(GetInput(filepath))
	nodes := getNodes(input)
	anodes := GetEmpty2dArr[bool](len(input), len(input[0]))

	for _, n := range nodes {
		for y, row := range input {
			for x, r := range row {
				if n.x == x && n.y == y { continue }
				if n.r == r {
					fillMap(anodes, n.y, n.x, y, x)
				}
			}
		}
	}
	return countTrue2d(anodes)
}

func fillMap(anodes [][]bool, y1, x1, y2, x2 int) {
	xD := x1 - x2
	yD := y1 - y2
	for y, x := x1, y1; InBounds(anodes, y, x); y, x = y+yD, x+xD {
		anodes[y][x] = true
	}
	for y, x := x1, y1; InBounds(anodes, y, x); y, x = y-yD, x-xD {
		anodes[y][x] = true
	}
}
