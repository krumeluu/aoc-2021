package day02

import (
	. "aoc/utils"
)

func Part1(filepath string) int {
	input := GetInput(filepath)
	reports := StrsAsInts2d(GetRowsByCols(input, " "))
	margin := 3
	var safe int

	for _, report := range reports {
		isIncr := report[0] < report[1]
		for i := 0; i < len(report)-1; i++ {
			a := report[i]
			b := report[i+1]
			if !((a < b) == isIncr && a != b && WithinMargin(a, b, margin)) {
				break
			}
			if i == len(report)-2 {
				safe++
			}
		}
	}
	return safe
}
