package day02

import (
	. "aoc/utils"
)

func Part2(filepath string) int {

	input := GetInput(filepath)
	reports := StrsAsInts2d(GetRowsByCols(input, " "))
	var safe int

	for _, report := range reports {
		for skip := 0; skip < len(report); skip++ {
			subReport := FilterIndex(report, skip)
			if isSafe(subReport) {
				safe++
				break
			}
		}
	}
	return safe
}

func isSafe(levels []int) bool {

	var margin = 3
	for i := 1; i < len(levels)-1; i++ {
		a := levels[i-1]
		b := levels[i]
		c := levels[i+1]
		if !(WithinMargin(a, b, margin) && WithinMargin(b, c, margin)) {
			return false
		}
		if !((a < b && b < c) || (a > b && b > c)) {
			return false
		}
	}
	return true
}
