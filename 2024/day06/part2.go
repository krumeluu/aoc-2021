package day06

import (
	. "aoc/utils"
	"slices"
)

var loops int

func Part2(filepath string) int {

	input := GetInput(filepath)
	area := ParseAsRunes2d(input)
	start := GetCoords(area, '^')

	if len(start) == 0 {
		panic("Didn't find starting coordinate")
	}

	g := Pos{
		Y: start[0][0],
		X: start[0][1],
		Dir: 3,
	}
	log := []Pos{}
	tried := []Coord{}

	for InBounds(area, g.Y, g.X) {

		if isObstacle(area, g) {
			g = goBack(g)
			g = turnRight(g)
			continue
		}

		nextG := goForward(g)

		if canTry(area, tried, nextG) {
			tryLayout := Copy2dSlice(area)							// Copy map layout
			logCopy := append([]Pos{}, log...)						// Copy log
			tryLayout[nextG.Y][nextG.X] = '#'						// Add obstacle
			tried = append(tried, Coord{Y: nextG.Y, X: nextG.X})	// Add position to list of tried
			if isLooping(tryLayout, logCopy, g) {
				loops++
			}
		}

		log = append(log, g) // Log position
		g = nextG	         // Move forward
	}

	return loops
}

func canTry(area [][]rune, tried []Coord, pos Pos) bool {
	if !InBounds(area, pos.Y, pos.X) ||						// Check that coordinate is in-bounds
	slices.Contains(tried, Coord{Y: pos.Y, X: pos.X}) ||	// Check that we haven't tried this before
		area[pos.Y][pos.X] == '#' ||						// Check that this isn't an obstacle already	
		area[pos.Y][pos.X] == '^' {							// Check that this isn't the guards starting position
			return false
		}
	return true
}

func isLooping(area [][]rune, log []Pos, g Pos) bool {

	for InBounds(area, g.Y, g.X) {
		if isObstacle(area, g) {
			g = goBack(g)
			g = turnRight(g)
			continue
		}
		g = goForward(g)
		if beenHere(log, g) {
			return true
		}
		log = append(log, g)
	}
	return false
}

func isObstacle(area [][]rune, g Pos) bool {
	return area[g.Y][g.X] == '#'
}

func goBack(g Pos) Pos {
	g.Y -= dirs[g.Dir].Y
	g.X -= dirs[g.Dir].X
	return g
}

func goForward(g Pos) Pos {
	g.Y += dirs[g.Dir].Y
	g.X += dirs[g.Dir].X
	return g
}

func turnRight(g Pos) Pos {
	g.Dir = (g.Dir + 1) % len(dirs)
	return g
}

func beenHere(log []Pos, g Pos) bool {
	for _, l := range log {
		if l.Y == g.Y && l.X == g.X && l.Dir == g.Dir {
			return true
		}
	}
	return false
}
