package day06

import (
	. "aoc/utils"
)

var dirs = []Coord {
    { Y: -1, X:  0},
    { Y:  0, X:  1},
    { Y:  1, X:  0},
	{ Y:  0, X: -1},
}

func Part1(filepath string) int {
	input := GetInput(filepath)
	arr := ParseAsRunes2d(input)
	visited := GetEmpty2dArr[bool](len(arr), len(arr[0]))
	start := GetCoords(arr, '^')
	if len(start) == 0 {
		panic("Didn't find starting coordinate")
	}

	g := Coord{
		Y: start[0][0],
		X: start[0][1],
	}
	sum := 0
	di := 0

	for InBounds(arr, g.Y, g.X) {
		if arr[g.Y][g.X] == '#' {
			g.Y -= dirs[di].Y
			g.X -= dirs[di].X
			di = (di + 1) % len(dirs)
			continue
		}
		if !visited[g.Y][g.X] {
			sum++
			visited[g.Y][g.X] = true
		}
		g.Y += dirs[di].Y
		g.X += dirs[di].X
	}
	return sum
}

