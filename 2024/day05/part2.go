package day05

import (
	"aoc/utils"
	"slices"
)

func Part2(filepath string) int {
	input := utils.GetInput(filepath)

	rules, updates := getRulesAndUpdates(input)
	fixedUpdates := getFixedUpdates(updates, rules)
	middleSum := getMiddleSum(fixedUpdates)

	return middleSum
}

func cleanRules(update []int, rules [][]int) [][]int {
	cleaned := make([][]int, 0)
	for _, rule := range rules {
		if slices.Contains(update, rule[0]) &&
		   slices.Contains(update, rule[1]) {
			   cleaned = append(cleaned, rule)
		}
	}
	return cleaned
}

func fixUpdate(update []int, rules [][]int) []int {

	rules = cleanRules(update, rules)
	for i := 0; i < len(update); i++ {
		for j := 0; j < len(rules); j++ {
			if update[i] == rules[j][0] {
				right := slices.Index(update, rules[j][1])
				if i > right {
					update[i], update[right] = update[right], update[i]
					i, j = 0, 0
				}
			}
		}
	}
	return update
}

func getFixedUpdates(updates, rules [][]int) [][]int {

	result := make([][]int, 0)
	for _, update := range updates {
		rulesLeft := len(rules)
		for i := 0; i < len(rules); i, rulesLeft = i+1, rulesLeft-1 {
			lowerF := slices.Index(update, rules[i][0])
			upperF := slices.Index(update, rules[i][1])
			if upperF == -1 || lowerF == -1 {
				continue
			}
			if upperF < lowerF {
				result = append(result, fixUpdate(update, rules))
				break
			}
		}
	}
	return result
}


