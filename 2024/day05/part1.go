package day05

import (
	. "aoc/utils"
	"slices"
)

func Part1(filepath string) int {

	input := GetInput(filepath)
	rules, updates := getRulesAndUpdates(input)
	validUpdates := getValidUpdates(updates, rules)
	middleSum := getMiddleSum(validUpdates)

	return middleSum
}


func getValidUpdates(updates, rules [][]int) [][]int {

	valids := make([][]int, 0)
	for _, update := range updates {
		rulesLeft := len(rules)
		for i := 0; i < len(rules); i, rulesLeft = i+1, rulesLeft-1 {
			loFound := slices.Index(update, rules[i][0])
			hiFound := slices.Index(update, rules[i][1])
			if hiFound == -1 || loFound == -1 {
				continue
			}
			if hiFound < loFound {
				break
			}
		}
		if rulesLeft == 0 {
			valids = append(valids, update)
		}
	}
	return valids
}

func getRulesAndUpdates(input string) ([][]int, [][]int) {

	blocks := SplitByEmptyNewline(input)
	ruleStrs := GetRowsByCols(blocks[0], `\|`)
	rules := StrsAsInts2d(ruleStrs)

	updateStrs := GetRowsByCols(blocks[1], ",")
	updates := StrsAsInts2d(updateStrs)

	return rules, updates
}


func getMiddleSum(updates[][]int) int {
	var sum int
	for _, update := range updates {
		sum += update[len(update)/2]
	}
	return sum
}
