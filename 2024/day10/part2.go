package day10

import (
	. "aoc/utils"
)
func Part2(filepath string) int {
	input := RunesAsInts2d(ParseAsRunes2d(GetInput(filepath)))
	trailheads := GetCoords(input, 0)
	sum := 0
	
	for _, tHead := range trailheads {
		sum += hikeRating(input, Coord{Y: tHead[1], X: tHead[0]})

	}

	return sum
}

func hikeRating(tMap [][]int, here Coord) int {

	if tMap[here.Y][here.X] == 9 {
		return 1
	}

	sum := 0

	for _, dir := range dirs {
		there := Coord{
			Y: here.Y + dir.Y,
			X: here.X + dir.X,
		}
		if ValidDir(tMap, here, there) {
			sum += hikeRating(tMap, there)
		}
	}

	return sum
}
