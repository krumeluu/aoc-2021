package day10

import (
	. "aoc/utils"
	"slices"
)

var dirs = []Coord {
	{ Y:  0, X:  1},
    { Y:  1, X:  0},
	{ Y:  0, X: -1},
	{ Y: -1, X:  0},
}

func Part1(filepath string) int {
	input := RunesAsInts2d(ParseAsRunes2d(GetInput(filepath)))
	trailheads := GetCoords(input, 0)
	sum := 0
	
	for _, tHead := range trailheads {
		peaks := hike(input, Coord{Y: tHead[1], X: tHead[0]})
		slices.SortFunc(peaks, SortCoords)
		peaks = slices.Compact(peaks)
		score := len(slices.Compact(peaks))
		sum += score

	}
	return sum
}

func hike(tMap [][]int, here Coord) []Coord {

	if tMap[here.Y][here.X] == 9 {
		return []Coord{here}
	}

	peaks := []Coord{}

	for _, dir := range dirs {
		var there Coord
		there.Y = here.Y + dir.Y
		there.X = here.X + dir.X
		if ValidDir(tMap, here, there) {
			peaks = append(peaks, hike(tMap, there)...)
		}
	}

	return peaks
}

func ValidDir(tMap [][]int, here, there Coord) bool {
	return InBounds(tMap, there.X, there.Y) &&
			tMap[there.Y][there.X] - tMap[here.Y][here.X] == 1
}
