package day11

import (
	. "aoc/utils"
	"strconv"
	"strings"
)

func Parts(filepath string, blinks int) int {
	input := StrsAsInts(strings.Split(GetInput(filepath), " "))
	sum := 0

	for _, n := range input {
		sum += blink(n, blinks)
	}

	return sum
}

var cache = map[string]int {}

func blink(n, blinks int) int {
	if blinks == 0 {
		return 1
	}
	key := strconv.Itoa(n)
	key += " " + strconv.Itoa(blinks)
	v, ok := cache[key]
	if ok {
		return v
	}
	if n == 0 {
		v = blink(1, blinks-1)
	} else if IntLen(n) % 2 == 0 {
		left, right := SplitInt(n)
		v = blink(left, blinks-1) + blink(right, blinks-1)
	} else {
		v = blink(n * 2024, blinks-1)
	}
	cache[key] = v
	return v
}
