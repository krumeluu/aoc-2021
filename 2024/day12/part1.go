package day12

import (
	. "aoc/utils"
)

func Part1(filepath string) int {
	garden := ParseAsRunes2d(GetInput(filepath))
	mapped := GetEmpty2dArr[bool](len(garden), len(garden[0]))
	regs := []Region{}

	for y, row := range garden {
		for x, plant := range row {
			if mapped[y][x] {
				continue
			}
			reg := Region {
				Type: plant,
			}
			reg.Area, reg.Perim, mapped = measureRegion(garden, mapped, Coord{Y: y, X: x})
			regs = append(regs, reg)
		}
	}

	var sum int
	for _, reg := range regs {
		sum += reg.Area * reg.Perim
	}

	return sum
}

func measureRegion(garden [][]rune, mapped [][]bool, pos Coord) (int, int, [][]bool) {
	mapped[pos.Y][pos.X] = true
	area := 1
	perim := 0
	for _, dir := range Dirs4 {
		nextY := pos.Y + dir.Y
		nextX := pos.X + dir.X
		if !InBounds(garden, nextY, nextX) || garden[nextY][nextX] != garden[pos.Y][pos.X] {
			perim++
		} else if !mapped[nextY][nextX] {
			var nextArea, nextPerim int
			nextArea, nextPerim, mapped = measureRegion(garden, mapped, Coord{Y: nextY, X: nextX})
			area += nextArea
			perim += nextPerim
		}
	}
	return  area, perim, mapped
}
