package day12

import (
	. "aoc/utils"
)

type Region struct {
	Type  rune
	Area  int
	Perim int
	Sides int
}

func Part2(filepath string) int {
	garden := ParseAsRunes2d(GetInput(filepath))
	mapped := GetEmpty2dArr[bool](len(garden), len(garden[0]))
	regs := []Region{}

	for y, row := range garden {
		for x, plant := range row {
			if mapped[y][x] { continue }
			here := Coord{Y: y, X: x}
			r := Region{ Type: plant }
			r, mapped = mapRegion(garden, r, here, mapped)
			regs = append(regs, r)
		}
	}

	var price int
	for _, r := range regs {
		price += r.Area * r.Sides
	}

	return price
}

func mapRegion(garden [][]rune, r Region, here Coord, mapped [][]bool) (Region, [][]bool) {

	mapped[here.Y][here.X] = true
	r.Area++
	r.Sides += countCorners(garden, here, r.Type)

	for _, dir := range Dirs4 {
		there := NextCoord(here, dir)
		if withinRegion(garden, there, r.Type) && !mapped[there.Y][there.X] {
			   r, mapped = mapRegion(garden, r, there, mapped)
		}
	}

	return r, mapped
}

func countCorners(garden [][]rune, pos Coord, plant rune) int {
	var count int
	for i, dir := range Dirs4 {
		neigh1 := NextCoord(pos, dir)
		neigh2 := NextCoord(pos, Dirs4[(i+1)%4])
		corner := NextCoord(pos, Corners[i])

		if isInsideCorner(garden, neigh1, neigh2, plant) {
			count++
		} else if isOutsideCorner(garden, neigh1, neigh2, corner, plant) {
			count++
		}
	}
	return count
}

func isOutsideCorner(garden [][]rune, n1, n2, corner Coord, plant rune) bool {
	return withinRegion(garden, n1, plant) &&
	       withinRegion(garden, n2, plant) &&
	      !withinRegion(garden, corner, plant)
}

func isInsideCorner(garden [][]rune, n1, n2 Coord, plant rune) bool {
	return !withinRegion(garden, n1, plant) &&
		   !withinRegion(garden, n2, plant)
}

func withinRegion(garden [][]rune, pos Coord, plant rune) bool {
	return InBounds(garden, pos.Y, pos.X) &&
		   garden[pos.Y][pos.X] == plant
}
