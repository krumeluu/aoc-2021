package day09

import (
	. "aoc/utils"
)

func Part1(filepath string) int {

	input := RunesAsInts([]rune(GetInput(filepath)))
	disk := make([]int, 0)
	sum := 0

	for i, end := 0, len(input)-1; input[end] > 0; {
		for input[i] > 0 {				// While there are file blocks
			input[i]--					// Subtrack block of file
			disk = append(disk, i/2)	// Add ID to disk
		}
		for input[end] > 0 && input[i+1] > 0 {
			disk = append(disk, end/2)	// Append ID of the last block
			input[end]--				// Subtrack block from the end
			input[i+1]--				// Subtrack empty space from dest
		}
		if input[end] == 0 {			// If all blocks are moved
			end -= 2					// Decrement index
		}
		if input[i+1] == 0 {			// If space has been filled
			i += 2						// Increment index
		}
	}

	for i := range disk {
		sum += disk[i] * i
	}

	return sum
}
