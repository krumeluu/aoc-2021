package day09

import (
	. "aoc/utils"
)

func Part2(filepath string) int {

	input := RunesAsInts([]rune(GetInput(filepath)))
	spans := make([][]int, len(input))
	checksum := 0

	for i, space := range input {
		spans[i] = make([]int, space)
		ID := i/2
		if i%2 == 1 {
			continue
		}
		for j := range space {
			spans[i][j] = ID
		}
	}

	for i := len(spans) - 1; i > 0; i -= 2 {
		for j := 1; j <= i; j += 2 {
			available, index := available(spans[j])
			if len(spans[i]) <= available {
				move(spans[i], spans[j][index:])
				break
			}
		}
	}

	index := 0
	for _, span := range spans {
		for _, ID := range span {
			checksum += ID * index
			index++
		}
	}

	return checksum
}

func move(from, to []int) {
	j := 0
	for i := range from {
		if to[i] == 0 {
			to[i], from[j] = from[j], to[i]
			j++
		}
	}
}

func available(s []int) (int, int) {
	space := 0
	index := 0
	for n := range s {
		if s[n] == 0 {
			space++
		} else {
			index ++
		}
	}
	return space, index
}
