package day13

import (
	. "aoc/utils"
	"fmt"
	"strconv"
)

type Machine struct {
	A Coord;
	B Coord;
	Prize Coord;
}

func Part1(filepath string) int {
	input := SplitByEmptyNewline(GetInput(filepath))
	machines := parseMachines(input)
	tokens := 0

	for i, m := range machines {
		cost := solveMachine(m)
		if cost < 0 {continue}
		tokens += cost
		fmt.Printf("Machine %d: %d tokens\n", i+1, cost)
	}
	return tokens
}

func solveMachine(m Machine) int {
	B := m.Prize.X / m.B.X
	for ; B >= 0; B-- {
		xDiff := m.Prize.X - B * m.B.X
		if !(xDiff % m.A.X == 0) {
			continue
		}
		A := xDiff / m.A.X
		if B * m.B.X + A * m.A.X == m.Prize.X &&
		   B * m.B.Y + A * m.A.Y == m.Prize.Y {
			   return A * 3 + B
		}
	}
	return -1
}

func parseMachines(machinesStr []string) []Machine {
	var machines = []Machine{}
	for _, mStr := range machinesStr {
		lines := GetRowsByCols(mStr, " ")
		m := Machine{}
		var x, y int
		for i, line := range lines {
			if i < 2 {
				xStr := line[2:][0][2:]
				yStr := line[2:][1][2:]
				x, _ = strconv.Atoi(xStr[:len(xStr)-1])
				y, _ = strconv.Atoi(yStr)
			} else {
				xStr := line[1:][0][2:]
				yStr := line[1:][1][2:]
				x, _ = strconv.Atoi(xStr[:len(xStr)-1])
				y, _ = strconv.Atoi(yStr)
			}
			switch i {
			case 0:
				m.A = Coord{Y: y, X: x}
			case 1:
				m.B = Coord{Y: y, X: x}
			default:
				m.Prize = Coord{Y: y, X: x}
			}
		}
		machines = append(machines, m)
	}
	return machines
}
