package main

import (
	"aoc/day01"
	"aoc/day02"
	"aoc/day03"
	"aoc/day04"
	"aoc/day05"
	"aoc/day06"
	"aoc/day07"
	"aoc/day08"
	"aoc/day09"
	"aoc/day10"
	"aoc/day11"
	"aoc/day12"
	"fmt"
)

// var input = "example.txt"
var input = "input.txt"

func main() {

	fmt.Printf("Day 1, Part 1: %d\n", day01.Part1("day01/"+input))
	fmt.Printf("Day 1, Part 2: %d\n", day01.Part2("day01/"+input))
	fmt.Println()

	fmt.Printf("Day 2, Part 1: %d\n", day02.Part1("day02/"+input))
	fmt.Printf("Day 2, Part 2: %d\n", day02.Part2("day02/"+input))
	fmt.Println()

	fmt.Printf("Day 3, Part 1: %d\n", day03.Part1("day03/"+input))
	fmt.Printf("Day 3, Part 2: %d\n", day03.Part2("day03/"+input))
	fmt.Println()

	fmt.Printf("Day 4, Part 1: %d\n", day04.Part1("day04/"+input))
	fmt.Printf("Day 4, Part 2: %d\n", day04.Part2("day04/"+input))
	fmt.Println()

	fmt.Printf("Day 5, Part 1: %d\n", day05.Part1("day05/"+input))
	fmt.Printf("Day 5, Part 2: %d\n", day05.Part2("day05/"+input))
	fmt.Println()

	fmt.Printf("Day 6, Part 1: %d\n", day06.Part1("day06/"+input))
	fmt.Printf("Day 6, Part 2: %d\n", day06.Part2("day06/"+input))
	fmt.Println()

	fmt.Printf("Day 7, Part 1: %d\n", day07.Part1("day07/"+input))
	fmt.Printf("Day 7, Part 2: %d\n", day07.Part2("day07/"+input))
	fmt.Println()

	fmt.Printf("Day 8, Part 1: %d\n", day08.Part1("day08/"+input))
	fmt.Printf("Day 8, Part 2: %d\n", day08.Part2("day08/"+input))
	fmt.Println()

	fmt.Printf("Day 9, Part 1: %d\n", day09.Part1("day09/"+input))
	fmt.Printf("Day 9, Part 2: %d\n", day09.Part2("day09/"+input))
	fmt.Println()

	fmt.Printf("Day 10, Part 1: %d\n", day10.Part1("day10/"+input))
	fmt.Printf("Day 10, Part 2: %d\n", day10.Part2("day10/"+input))
	fmt.Println()

	fmt.Printf("Day 11, Part 1: %d\n", day11.Parts("day11/"+input, 25))
	fmt.Printf("Day 11, Part 2: %d\n", day11.Parts("day11/"+input, 75))
	fmt.Println()

	fmt.Printf("Day 12, Part 1: %d\n", day12.Part1("day12/"+input))
	fmt.Printf("Day 12, Part 2: %d\n", day12.Part2("day12/"+input))
	fmt.Println()

	// fmt.Printf("Day 13, Part 1: %d\n", day13.Part1("day13/"+input))
	// fmt.Printf("Day 13, Part 2: %d\n", day13.Part2("day13/"+input))
	// fmt.Println()
}
