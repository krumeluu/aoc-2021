package day04

import (
	. "aoc/utils"
)

func Part2(filepath string) int {

	input := GetInput(filepath)
	arr := ParseAsRunes2d(input)
	count := 0

	for y := 1; y < len(arr)-1; y++ {
		for x := 1; x < len(arr[y])-1; x++ {
			if arr[y][x] == 'A' && FindMasX(arr, y, x) {
				count++
			}
		}
	}
	return count
}

func FindMasX(a [][]rune, y, x int) bool {

	return (a[y-1][x-1] == 'M' && a[y+1][x+1] == 'S'  ||
		    a[y-1][x-1] == 'S' && a[y+1][x+1] == 'M') &&
		   (a[y+1][x-1] == 'M' && a[y-1][x+1] == 'S'  ||
			a[y+1][x-1] == 'S' && a[y-1][x+1] == 'M')

}
