package day04

import (
	. "aoc/utils"
)

func Part1(filepath string) int {
	input := GetInput(filepath)
	arr := ParseAsRunes2d(input)
	word := []rune("XMAS")
	count := 0

	for y, row := range arr {
		for x := range row {
			count += FindPatternFromCoord(arr, word, y, x)
		}
	}
	return count
}

func FindPatternFromCoord[T any](arr [][]T, pattern []T, yStart, xStart int) int {

	var dirs = [][]int{
		{-1, -1},
		{-1,  0},
		{-1,  1},
		{ 0, -1},
		{ 0,  1},
		{ 1, -1},
		{ 1,  0},
		{ 1,  1},
	}

	var found int

	for _, dir := range dirs {
		y, x := yStart, xStart
		toFind := len(pattern)
		for _, c := range pattern {
			if !InBounds(arr, y, x) ||
			   !Equal(arr[y][x], c) {
				break
			}
			toFind--
			y += dir[0]
			x += dir[1]
		}
		if toFind == 0 {
			found++
		}
	}
	return found
}
