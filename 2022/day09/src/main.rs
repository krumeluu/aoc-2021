use std::fs::read_to_string;

const INPUT: &str = "input";
// const INPUT: &str = "example_input";

#[derive(Debug)]
enum Dir {
    U,
    D,
    R,
    L,
}

fn main() {
    let input = read_to_string(INPUT).expect("Couldn't read string");
    println!("PART 1: {}", part_x(&input, 2));
    println!("PART 2: {}", part_x(&input, 10));
}

fn too_far(head: &(i32, i32), tail: &(i32, i32)) -> bool {
    (head.0 - tail.0).abs() > 1 || (head.1 - tail.1).abs() > 1
}

fn move_tail(head: &(i32, i32), tail: &mut (i32, i32)) {
    tail.0 += (head.0 - tail.0).signum();
    tail.1 += (head.1 - tail.1).signum();
}

fn move_head(head: &mut (i32, i32), dir: &Dir) {
    match dir {
        Dir::U => head.0 += 1,
        Dir::D => head.0 -= 1,
        Dir::R => head.1 += 1,
        Dir::L => head.1 -= 1,
    }
}

fn part_x(input: &str, rope_sz: usize) -> usize {
    let mut rope = vec![(0, 0); rope_sz];
    let mut history: Vec<(i32, i32)> = Vec::new();
    history.push((0, 0));

    for line in input.lines() {
        let (dir, amount) = line.split_once(' ').unwrap();
        let amount = amount.parse::<i32>().unwrap();

        let dir = match dir {
            "U" => Dir::U,
            "D" => Dir::D,
            "R" => Dir::R,
            "L" => Dir::L,
            _ => panic!("Invalid input!"),
        };

        for _ in 0..amount {
            move_head(&mut rope[0], &dir);
            for i in 1..rope.len() {
                if too_far(&rope[i-1], &rope[i]) {
                    move_tail(&rope[i-1].clone(), &mut rope[i]);
                } else {
                    break;
                }
            }
            if rope[rope.len()-1] != *history.last().unwrap() {
                history.push(rope[rope.len()-1]);
            }
        }
    }

    history.sort();
    history.dedup();
    history.len()
}
