use std::fs::read_to_string;

// const FILE: &str = "example_input";
const FILE: &str = "input";

fn main() {
    let input = read_to_string(FILE)
        .expect("Couldn't open file");

    println!("{}", part_x(&input, 1));
    println!("{}", part_x(&input, 2));
}

fn part_x(input: &str, part: u8) -> String {
    let (stacks, instrs) = input.split_once("\n\n").unwrap();
    let mut stacks = parse_stacks(&stacks);
    let instrs = parse_instrs(&instrs);
    let mut result = String::new();
    match part {
        1 => move_stacks(&instrs, &mut stacks),
        2 => move_stacks_9001(&instrs, &mut stacks),
        _ => panic!("No such part! Only 1 and 2 available"),
    }

    for stack in stacks {
        let value = stack.get(stack.len()-1);
        let value = match value {
            None => ' ',
            Some(value) => *value,
        };
        result.push(value);
    };

    result
}

fn parse_stacks(stacks: &str) -> Vec<Vec<char>> {

    let lines = stacks.lines().rev().skip(1);
    let mut all_stacks: Vec<Vec<char>> = Vec::new();

    for (i, line) in lines.enumerate() {
        let len = (line.len() + 1) / 4 as usize;

        if i == 0 {
            for _ in 0..len {
                all_stacks.push(Vec::new());
            }
        }

        for j in 0..len {
            let start = j*4;
            let item = line[start..start+3].chars().nth(1).filter(|x| x.is_alphabetic());

            match item {
                Some(value) => all_stacks[j].push(value),
                None => continue,
            }
        }
    }
    all_stacks
}

fn parse_instrs(instrs: &str) -> Vec<Vec<u8>> {
    let mut instr_rows: Vec<Vec<u8>> = Vec::new();
    for line in instrs.lines() {
        let mut instr_arr = Vec::new();
        for word in line.split(' ').skip(1).step_by(2) {
            instr_arr.push(word.parse::<u8>().unwrap());
        }
        instr_rows.push(instr_arr);
    }
    instr_rows
}

fn move_stacks(instrs: &Vec<Vec<u8>>, stacks: &mut Vec<Vec<char>>) {
    for instr_row in instrs {
        let amount = instr_row[0];
        let from = instr_row[1] as usize - 1;
        let to = instr_row[2] as usize - 1;
        for _ in 0..amount {
            let crane = stacks[from].pop();
            match crane {
                Some(value) => stacks[to].push(value),
                None => continue,
            }
        }

    };
}

fn move_stacks_9001(instrs: &Vec<Vec<u8>>, stacks: &mut Vec<Vec<char>>) {
    for instr_row in instrs {
        let amount = instr_row[0];
        let mut tmp_stack = Vec::new();
        let from = instr_row[1] as usize - 1;
        let to = instr_row[2] as usize - 1;
        for _ in 0..amount {
            let crane = stacks[from].pop();
            match crane {
                Some(value) => tmp_stack.push(value),
                None => continue,
            }
        }
        for stack in tmp_stack.into_iter().rev() {
            stacks[to].push(stack);
        }
    };
}


