use std::collections::HashSet;
use std::fs;

const FILE: &str = "input";
// const FILE: &str = "example_input";

fn main() {
    println!("PART 1: {}", part_1());
    println!("PART 2: {}", part_2());
}

fn part_1() -> u32 {
    let input = fs::read_to_string(FILE).expect("Couldn't read file");
    let mut acc: u32 = 0;

    for line in input.lines() {
        let halfway = line.len() / 2;
        let first_half: String = line[..halfway].to_string();
        let second_half: String = line[halfway..].to_string();

        let c = first_half
            .chars()
            .filter(|&n| second_half.contains(n))
            .collect::<Vec<char>>();

        acc += char_to_int(c[0]);
    }

    acc
}

fn part_2() -> u32 {
    let input = fs::read_to_string(FILE).expect("Couldn't read file");

    let mut i = 0;
    let mut acc = 0;
    let mut triple_line = String::new();

    for line in input.lines() {
        triple_line.push_str(&remove_duplicates(line.to_string()));
        i += 1;
        if i < 3 {
            continue;
        }

        let mut triple_arr = triple_line.chars().collect::<Vec<char>>();
        let mut count = 1;
        let mut prev: char = ' ';
        triple_arr.sort();

        for c in triple_arr {
            if c == prev {
                count += 1;
            } else {
                count = 1;
            }
            if count > 2 {
                prev = c;
                break;
            }
            prev = c;
        }

        acc += char_to_int(prev);
        triple_line.clear();
        i = 0;
    }

    acc
}

fn char_to_int(c: char) -> u32 {
    if c.is_ascii_lowercase() {
        c as u32 - 'a' as u32 - 1
    } else {
        c as u32 - 'A' as u32 - 1 + 26
    }
}

fn remove_duplicates(mut s: String) -> String {
    let mut seen = HashSet::new();
    s.retain(|c| seen.insert(c));
    s
}
