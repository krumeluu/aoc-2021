use std::fs;

const FILE: &str = "input";
// const FILE: &str = "example_input";

fn main() {
    println!("PART 1: {:?}", part_1());
    println!("PART 2: {:?}", part_2());
}

fn part_1() -> u32 {

    let input = fs::read_to_string(FILE)
        .expect("Should have been able to read input file");

    let mut elfs: [u32; 256] = [0; 256];
    let mut i: usize = 0;

    for line in input.lines() {
        if line.is_empty() {
            i += 1;
            continue;
        }
        let num = line.parse::<u32>();
        let num = match num {
            Ok(num)  => num,
            Err(_) => panic!(),
        };
        elfs[i] += num;
    }

    let max = elfs.iter().max();
    match max {
        Some(max) => return *max,
        None => panic!(),
    }
}

fn part_2() -> u32 {

    let input = fs::read_to_string(FILE)
        .expect("Should have been able to read input file");

    let mut elfs: [u32; 256] = [0; 256];
    let mut i: usize = 0;

    for line in input.lines() {
        if line.is_empty() {
            i += 1;
            continue;
        }
        let num = line.parse::<u32>();
        let num = match num {
            Ok(num)  => num,
            Err(_) => panic!(),
        };
        elfs[i] += num;
    }

    elfs.sort();
    elfs.reverse();
    elfs[(0..3)].into_iter().sum()
}
