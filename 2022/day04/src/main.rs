use std::fs::read_to_string;

const FILE: &str = "input";
// const FILE: &str = "example_input";

fn main() {
    println!("{}", part_1());
    println!("{}", part_2());
}

fn part_1() -> u32 {

    let input = read_to_string(FILE)
        .expect("Couldn't open file");
    let mut count = 0;

    for line in input.lines() {
        let (r1, r2) = line.split_once(',').unwrap();
        let (r1_lo, r1_hi) = r1.split_once('-').unwrap();
        let (r2_lo, r2_hi) = r2.split_once('-').unwrap();
        let r1_lo = r1_lo.parse::<u32>().unwrap();
        let r1_hi = r1_hi.parse::<u32>().unwrap();
        let r2_lo = r2_lo.parse::<u32>().unwrap();
        let r2_hi = r2_hi.parse::<u32>().unwrap();

        if (r1_lo..=r1_hi).contains(&r2_lo) && (r1_lo..=r1_hi).contains(&r2_hi) {
            count += 1;
        } else if (r2_lo..=r2_hi).contains(&r1_lo) && (r2_lo..=r2_hi).contains(&r1_hi) {
            count += 1;
        }
    }

    count
}

fn part_2() -> u32 {

    let input = read_to_string(FILE)
        .expect("Couldn't open file");
    let mut count = 0;

    for line in input.lines() {
        let (r1, r2) = line.split_once(',').unwrap();
        let (r1_lo, r1_hi) = r1.split_once('-').unwrap();
        let (r2_lo, r2_hi) = r2.split_once('-').unwrap();
        let r1_lo = r1_lo.parse::<u32>().unwrap();
        let r1_hi = r1_hi.parse::<u32>().unwrap();
        let r2_lo = r2_lo.parse::<u32>().unwrap();
        let r2_hi = r2_hi.parse::<u32>().unwrap();

        if (r1_lo..=r1_hi).contains(&r2_lo) || (r1_lo..=r1_hi).contains(&r2_hi) {
            count += 1;
        } else if (r2_lo..=r2_hi).contains(&r1_lo) || (r2_lo..=r2_hi).contains(&r1_hi) {
            count += 1;
        }
    }

    count
}
