use std::fs::read_to_string;

const INPUT: &str = "input";
const Y_CAP: usize = 41;
const X_CAP: usize = 1132;
const DEBUG: bool = false;
// const INPUT: &str = "example_input";
// const Y_CAP: usize = 5;
// const X_CAP: usize = 8;
// const DEBUG: bool = false;

const ESC: char = 27u8 as char;

#[derive(Clone, Copy, Debug)]
struct Location {
    visited: i32,
    alt: u8,
    is_end: bool,
}

impl Location {
    fn new() -> Self {
        Self {
            visited: -1,
            alt: b'a',
            is_end: false,
        }
    }
}

#[derive(PartialEq)]
enum Part {
    One,
    Two,
}

fn main() {
    let input = read_to_string(INPUT).expect("Couldn't read file!");
    println!("PART 1: {}", part_x(&input, Part::One));
    println!("PART 2: {}", part_x(&input, Part::Two));
}

fn part_x(input: &str, part: Part) -> i32 {
    let mut _dummy = String::new();
    let mut map = [[Location::new(); X_CAP]; Y_CAP];
    let mut starts: Vec<(usize, usize)> = Vec::new();
    let mut dists = Vec::new();

    for (y, line) in input.lines().enumerate() {
        for (x, byte) in line.as_bytes().iter().enumerate() {
            if *byte == b'S' {
                map[y][x].visited = 0;
                map[y][x].alt = b'a';
                starts.push((y, x));
            } else if *byte == b'E' {
                map[y][x].is_end = true;
                map[y][x].alt = 'z' as u8 + 1;
            } else if *byte == b'a' && part == Part::Two {
                map[y][x].visited = 0;
                starts.push((y, x));
            } else {
                map[y][x].alt = *byte;
            }
        }
    }

    for s in starts {
        let r = find(&mut map, s);
        if r.is_some() {
            dists.push(r.unwrap());
        }
        reset_map(&mut map);
    }

    *dists.iter().min().unwrap_or(&0)
}

fn reset_map(map: &mut [[Location; X_CAP]; Y_CAP]) {

    for y in 0..Y_CAP {
        for x in 0..X_CAP {
            if map[y][x].alt == b'a' {
                map[y][x].visited = 0;
            } else {
                map[y][x].visited = -1;
            }
        }
    }
}

#[derive(Debug)]
enum Dir {
    N,
    E,
    S,
    W,
}
const DIRS: [Dir; 4] = [Dir::N, Dir::E, Dir::S, Dir::W];

fn find(map: &mut [[Location; X_CAP]; Y_CAP], start: (usize, usize)) -> Option<i32> {

    let mut queue = Vec::new();
    queue.push(start);

    while !queue.is_empty() {

        if DEBUG {
            print_map(&map);
            step();
            clear_screen();
        }

        let vertex = queue.remove(0);
        let mut children = Vec::new();
        let (old_y, old_x) = vertex;

        if map[old_y][old_x].is_end == true {
            return Some(map[old_y][old_x].visited)
        }

        for dir in DIRS {

            if let Some(zygote) = valid_coord(vertex, dir) {
                let (new_y, new_x) = (zygote.0, zygote.1);
                if !(map[old_y][old_x].alt + 1>= map[new_y][new_x].alt) || map[new_y][new_x].visited > 0
                { continue; }
                children.push(zygote);
                map[new_y][new_x].visited = map[old_y][old_x].visited + 1;
            }
        }
        queue.extend(children);
    }
    None
}

fn valid_coord(coord: (usize, usize), dir: Dir) -> Option<(usize, usize)>{

    let zygote = match dir {
        Dir::N => (coord.0.checked_sub(1), Some(coord.1)),
        Dir::E => (Some(coord.0), coord.1.checked_add(1)),
        Dir::S => (coord.0.checked_add(1), Some(coord.1)),
        Dir::W => (Some(coord.0), coord.1.checked_sub(1)),
    };

    if zygote.0.is_none()
        || zygote.1.is_none()
        || zygote.0.unwrap() >= Y_CAP
        || zygote.1.unwrap() >= X_CAP
    { return None; }

    Some((zygote.0.unwrap(), zygote.1.unwrap()))
}

fn print_map(map: &[[Location; X_CAP]; Y_CAP]) {
    for y in 0..Y_CAP {
        for x in 0..X_CAP {
            let k = map[y][x];
            match k.visited >= 0 {
                true => print!("[{}]", char::from(k.alt)),
                false => print!(" {} ", char::from(k.alt)),
            }
        }
        println!();
    }
}

fn clear_screen() {
    for _ in 0..Y_CAP+1 {
        print!("{}{}", up(), erase());
    }
}

fn step() {
    let mut dummy = String::new();
    std::io::stdin().read_line(&mut dummy).unwrap();
}

fn up() -> String {
    format!("{}[A", ESC)
}

fn erase() -> String {
    format!("{}[2K", ESC)
}

