use std::fs::read_to_string;

// const FILE: &str = "example_input";
const FILE: &str = "input";

fn main() {
    let input = read_to_string(FILE)
        .expect("Couldn't read file");

    println!("Part 1: {}", part_x(&input, 4).unwrap());
    println!("Part 2: {}", part_x(&input, 14).unwrap());
}

fn part_x(input: &str, size: usize) -> Option<usize> {

    let mut result = None;

    'input: for i in 0..input.as_bytes().len() - 1 - size { // subtract size to avoid out-of-bounds at the end

        // println!();
        let slice = &input[i..i+size].as_bytes();
        let i_end = slice.len() - 1;

        for j in 0..=i_end {        // char in slice
            for k in j+1..=i_end {  // char in subslice
                // println!("{} == {} ? : {}", slice[j] as char, slice[k] as char, slice[j] == slice[k]);
                if slice[j] == slice[k] {
                    continue 'input;
                }
            }
        }
        result = Some(i + size);
        break;
    }

    result
}
