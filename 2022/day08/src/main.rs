
use std::fs::read_to_string;

// const FILE: &str = "input";
const FILE: &str = "example_input";

fn main() {
    let input = read_to_string(FILE)
        .expect("Couldn't read file");
    let (part_1, part_2) = part_x(&input);
    println!("\n\nPart 1: {}\nPart 2: {}", part_1, part_2);
}

#[derive(Debug)]
struct Tree {
    height: usize,
    seen: bool,
    coordinates: (usize, usize),
}

enum Dir {
    East,
    West,
    North,
    South,
}

impl Tree {

    fn scenic_score(&self, forest: &Vec<Tree>, cols: usize) -> usize {
        self.view(forest, cols, Dir::East)
            * self.view(forest, cols, Dir::West)
            * self.view(forest, cols, Dir::North)
            * self.view(forest, cols, Dir::South)
    }

    fn view(&self, forest: &Vec<Tree>, cols: usize, dir: Dir) -> usize {

        let mut view = 0;
        let height = self.height;
        let (y, x) = self.coordinates;
        let mut flat_x = y * cols + x;

        match dir {
            Dir::East => {
                while flat_x % cols != 0 {
                    flat_x -= 1;
                    view += 1;
                    if height <= forest[flat_x].height {
                        break;
                    }
                }
            },
            Dir::West => {
                while flat_x % cols != cols - 1 {
                    flat_x += 1;
                    view += 1;
                    if height <= forest[flat_x].height {
                        break;
                    }
                }
            },
            Dir::North => {
                while flat_x >= forest.len() / cols {
                    flat_x -= cols;
                    view += 1;
                    if height <= forest[flat_x].height {
                        break;
                    }
                }
            },
            Dir::South => {
                while flat_x < forest.len() - cols {
                    flat_x += cols;
                    view += 1;
                    if height <= forest[flat_x].height {
                        break;
                    }
                }
            }
        };
        view
    }
}

fn part_x(input: &str) -> (usize, usize) {

    let cols  = input.find('\n').unwrap();
    let input = input.replace("\n", "");
    let rows  = input.len() / cols;
    let mut forest: Vec<Tree> = Vec::new();

    for (i, c) in input.char_indices() {
        if c != '\n' {
            forest.push(Tree {
                height: c.to_digit(10).unwrap() as usize,
                seen: false,
                coordinates: (i/rows, i%rows),
            });
        }
    }

    // Part 1 //////////////////////////////////////////////////////
                                                                  //
    let mut prevs: [Option<usize>; 4] = [None; 4];                //
                                                                  //
    for i in 0..forest.len() {                                    //
                                                                  //
       if i % rows == 0 {                                         //
            prevs = [None; 4];                                    //
        }                                                         //
                                                                  //
        let dirs: [usize; 4] = [                                  //
            i,                                   // left to right //
            rows*cols-1-i,                       // right to left //
            i*rows%(rows*cols-1),                // top to bottom //
            (rows*cols-1)-(i*rows%(rows*cols-1)) // bottom to top //
        ];                                                        //
                                                                  //
        for (prev, dir) in prevs.iter_mut().zip(dirs.iter()) {    //
            if prev.is_none() {                                   //
                *prev = Some(forest[*dir].height);                //
                forest[*dir].seen = true;                         //
            } else if forest[*dir].height > prev.unwrap() {       //
                *prev = Some(forest[*dir].height);                //
                forest[*dir].seen = true;                         //
            }                                                     //
        }                                                         //
                                                                  //
                                                                  //
    }                                                             //
                                                                  //
    let mut result_1 = 0;                                         //
                                                                  //
    for (i, tree) in forest.iter().enumerate() {                  //
        if i % rows == 0 { println!(); }                          //
        if tree.seen == true {                                    //
            result_1 += 1;                                        //
            print!(" {} ", tree.height);                          //
        } else {                                                  //
            print!("[{}]", tree.height);                          //
        }                                                         //
    }                                                             //
                                                                  //
    ////////////////////////////////////////////////////////////////

    // Part 2 //////////////////////////////////////////////////////
                                                                  //
    let mut result_2 = 0;                                         //
                                                                  //
    for tree in &forest {                                         //
        let scenic_score = tree.scenic_score(&forest, cols);      //
        if scenic_score > result_2 { result_2 = scenic_score; }   //
    }                                                             //
                                                                  //
    ////////////////////////////////////////////////////////////////

    (result_1, result_2)
}

