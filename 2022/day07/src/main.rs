use std::fs::read_to_string;

// const FILE: &str = "example_input";
const FILE: &str = "input";


fn main() {
    let input = read_to_string(FILE)
        .expect("Couldn't read file");

    println!("Part 1: {}", part_1(&input));
    println!("Part 2: {}", part_2(&input));
}

fn parse_input(
    input: &str,
    ) -> Vec<(String, usize)> {

    let mut nodes: Vec<(String, usize)> = vec![(String::from("/"), 0)];
    let mut path = String::new();
    for line in input.lines() {
        if line.starts_with("$ cd") {
            let name = line.split_whitespace().last().unwrap();
            if name == "/" {
                path = "/".to_string();
            } else if name == ".." {
                path.pop();
                while path.chars().last().unwrap().is_alphabetic() {
                    path.pop();
                }
            } else {
                path.push_str(name);
                path.push('/');
            }
        } else if line == "$ ls" {
            continue;
        } else {
            if line.starts_with("dir") {
                let (_, name) = line.split_once(' ').unwrap();
                nodes.push((format!("{}{}/", path, name), 0));
            } else {
                let (size, name) = line.split_once(' ').unwrap();
                nodes.push((format!("{}{}", path, name), size.parse::<usize>().unwrap()));
            }
        }
    }
    nodes
}

fn measure_dirs(nodes: &Vec<(String, usize)>) -> Vec<usize> {
    let mut dirs: Vec<usize> = Vec::new();

    for dir in nodes.iter().filter(|x| x.1 == 0) {
        let dir_name = &dir.0;
        let mut sum = 0;
        for file in nodes.iter().filter(|y| y.1 != 0) {
            let file_name = &file.0;
            if  file_name.contains(dir_name) {
                sum += file.1;
            }
        }
        dirs.push(sum);
    }
    dirs
}

fn part_1(input: &str) -> usize {

    let nodes = parse_input(input);
    let dirs = measure_dirs(&nodes);
    dirs.iter().filter(|&x| *x <= 100000).sum()
}

fn part_2(input: &str) -> usize {

    let capacity = 70000000;
    let needed_for_update = 30000000;
    let nodes = parse_input(input);
    let dirs = measure_dirs(&nodes);
    let need_to_erase = needed_for_update - (capacity - dirs.iter().max().unwrap());
    dirs.into_iter().filter(|&x| x >= need_to_erase).min().unwrap()

}

