use std::fs::read_to_string;

const FILE: &str = "input";
// const FILE: &str = "example_input";

fn main() {
    let input = read_to_string(FILE)
        .expect("Couldn't read file!");
    println!("Part 1: {}", part_x(&input, 1, 20));
    println!("Part 2: {}", part_x(&input, 2, 10000));
}

#[derive(Debug)]
struct Monke {
    items: Vec<usize>,
    op: (Operand, Operand, Op),
    test: usize,
    to: (usize, usize),
    monke_bs: usize,
}

impl Monke {

    fn test(&self, item: &usize) -> bool {
        item % self.test == 0
    }

    fn op(&self, old: &usize) -> usize {
        let lhs: usize = match self.op.0 {
            Operand::Old => *old,
            Operand::New(v) => v,
        };
        let rhs: usize = match self.op.1 {
            Operand::Old => *old,
            Operand::New(v) => v,
        };
        match self.op.2 {
            Op::Add => lhs + rhs,
            Op::Sub => lhs - rhs,
            Op::Mul => lhs * rhs,
            Op::Div => lhs / rhs,
        }
    }

}


#[derive(Debug)]
enum Op {
    Add,
    Sub,
    Mul,
    Div,
}

#[derive(Debug)]
enum Operand {
    Old,
    New(usize),
}

fn get_items(line: &str) -> Vec<usize> {
    let mut v = Vec::new();
    let (_, items) = line.split_once(":").unwrap();
    let trim: &[_] = &[' ', ','];
    for n in items.split_whitespace() {
        v.push(n.trim_matches(trim).parse::<usize>().unwrap_or(0));
    }
    v
}

fn get_ops(line: &str) -> (Operand, Operand, Op) {
    let (_, op) = line.split_once('=').unwrap();
    let v: Vec<&str> = op.split_whitespace().collect();
    let lhs = v[0].parse::<usize>();
    let rhs = v[2].parse::<usize>();

    let lhs = match lhs {
        Ok(v) => Operand::New(v),
        Err(_) => Operand::Old,
    };
    let rhs = match rhs {
        Ok(v) => Operand::New(v),
        Err(_) => Operand::Old,
    };
    let op = match v[1] {
        "+" => Op::Add,
        "-" => Op::Sub,
        "*" => Op::Mul,
        "/" => Op::Div,
        _ => panic!("Tried to find valid operation, but found none"),
    };

    (lhs, rhs, op)
}

fn get_last_num(line: &str) -> usize {
    line
        .split_whitespace()
        .last()
        .unwrap_or("0")
        .parse::<usize>()
        .unwrap()
}

fn lcm(a: usize, b: usize) -> usize {
    a * b / gcd_lp(a, b)
}

fn gcd_lp(mut a: usize, mut b: usize) -> usize {

    let mut rem: usize = a % b;

    while rem != 0 {
            a = b;
            b = rem;
            rem = a % b;
    }

    b
}

fn part_x(input: &str, part: u8, rounds: usize) -> usize {

    let mut monkes: Vec<Monke> = Vec::new();

    ////////////////////////////////////
    // Parse input into Vec of Monkes //
    ////////////////////////////////////

    for monke in input.split("\n\n") {
        let mut lines = monke.lines().skip(1);

        let items = get_items(lines.next().unwrap_or(""));
        let op = get_ops(lines.next().unwrap_or(""));
        let test = get_last_num(lines.next().unwrap_or(""));
        let to_true = get_last_num(lines.next().unwrap_or(""));
        let to_false = get_last_num(lines.next().unwrap_or(""));
        let to = (to_true, to_false);

        let monke = Monke {
            items,
            op,
            test,
            to,
            monke_bs: 0,
        };

        monkes.push(monke);
    }


    let monkes_len = monkes.len();
    let mut shrink = monkes[0].test;

    for m in &monkes {                                  ////////////////////////////////
        shrink = lcm(m.test, shrink); //////////////////// Find Least Common Multiple of
    }                                                   // all the test numbers.
                                                        ////////////////////////////////
    ////////////////////////////////
    // Start Monkey in the middle //
    ////////////////////////////////

    for _ in 0..rounds {
        for m in 0..monkes_len {
            let items_len = monkes[m].items.len();
            for i in 0..items_len {
                let mut item = monkes[m].items[i];
                item = monkes[m].op(&item);
                if part == 1 {
                    item /= 3;
                }                                       ///////////////////////////////////////////
                item %= shrink; ////////////////////////// Shrink item down to the remainder of LCM
                monkes[m].monke_bs += 1;                ///////////////////////////////////////////
                match monkes[m].test(&item) {
                    true => {
                        let next = monkes[m].to.0;
                        monkes[next].items.push(item);
                    },
                    false => {
                        let next = monkes[m].to.1;
                        monkes[next].items.push(item);
                    },
                }
            }
        monkes[m].items.clear();
        }
    }

    monkes.sort_by_key(|m| m.monke_bs);
    monkes[monkes_len - 1].monke_bs * monkes[monkes_len - 2].monke_bs
}
