use std::fs::read_to_string;

const INPUT: &str = "input";
// const INPUT: &str = "example_input";

fn main() {
    let input = read_to_string(INPUT)
        .expect("Couldn't read input!");
    print!("Part 2:");
    println!("Part 1: {}", part_x(&input));
}

#[derive(PartialEq)]
enum Op {
    Noop,
    Addx,
}

fn part_x(input: &str) -> i32 {

    let mut cycle: usize = 0;
    let mut x: i32 = 1;
    let mut sum: i32 = 0;

    for line in input.lines() {

        draw_px(x, cycle);
        cycle += 1;

        let (op, value) = line.split_once(' ').unwrap_or(("noop", "0"));
        let value = value.parse::<i32>().unwrap();

        let op = match op {
            "noop" => Op::Noop,
            "addx" => Op::Addx,
            _ => unreachable!(),
        };

        if (cycle + 20) % 40 == 0 {
            let signal_strength= x * cycle as i32;
            sum += signal_strength;
        }

        if op == Op::Noop { continue; }

        draw_px(x, cycle);
        cycle += 1;

        if (cycle + 20) % 40 == 0 {
            let signal_strength= x * cycle as i32;
            sum += signal_strength;
        }

        x += value;
    }

    println!();
    sum
}

fn draw_px(sprite_x: i32, cycle: usize) {

    let x = cycle % 40;

    if x == 0 {
        println!();
    }
    match sprite_x - x as i32 {
            -1..=1 => print!("#"),
            _ => print!("."),
    }
}


