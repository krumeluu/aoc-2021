use std::fs;

// const FILE: &str = "example_input";
const FILE: &str = "input";

fn main() {
    println!("PART 1: {:?}", part_1());
    println!("PART 2: {:?}", part_2());
}

fn part_1() -> usize {

    let input = fs::read_to_string(FILE)
        .expect("Couldn't read file");

    let mut acc = 0;
    let scores = [ "B X",
                   "C Y",
                   "A Z",
                   "A X",
                   "B Y",
                   "C Z",
                   "C X",
                   "A Y",
                   "B Z",
                 ];

    for line in input.lines() {
        acc += scores
            .iter()
            .position(|&x| x == line)
            .unwrap_or(0) + 1;
    };

    acc
}

fn part_2() -> usize {

    let input = fs::read_to_string(FILE)
        .expect("Couldn't read file");

    let mut acc = 0;
    let scores = [ "B X",
                   "C X",
                   "A X",
                   "A Y",
                   "B Y",
                   "C Y",
                   "C Z",
                   "A Z",
                   "B Z",
                 ];

    for line in input.lines() {
        acc += scores
            .iter()
            .position(|&x| x == line)
            .unwrap_or(0) + 1;
    };

    acc
}
